

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BackendapiService } from '../services/backendapi.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'app-sub-categories',
	templateUrl: './sub-categories.component.html',
	styleUrls: ['./sub-categories.component.css']
})
export class SubCategoriesComponent implements OnInit {
	subCategoriesArray: any;
	id: any;
	name: any;
	formGroup = new FormGroup({});
	imageDirectoryPath: any = 'https://enacteservices.net/tinyprepbackend/';
	subCategories_name: any;
	subCategories_image: any;
	subCategories_cat_id: any;
	subCategories_id: any;
    subcategory_search: any = "";
	loading: boolean = false;
    loadingSpinner:boolean = false;
	messageNotFound: any;
	if_records: boolean = true;

	constructor(private api: BackendapiService, private router: Router, private http: HttpClient, private route: ActivatedRoute,
		private _fb: FormBuilder) {

		this.subcatLists();
		
	}

	ngOnInit(): void {
		this.formGroup = this._fb.group({
			'subCategories_name': ['', [Validators.required, Validators.minLength(3)]],
			'subCategories_image': ['', [Validators.required]]
		})
	}




	subcatLists() {

        this.loadingSpinner = true;   //    page load spinner 
		this.route.queryParams.subscribe((params: { [x: string]: string | undefined; }) => {
			this.id = params['id'];
			this.name = params['name'];
		});

		console.log(this.name);
		console.log(this.id);
		var data = {};
		data = {
			id: this.id,
			name:this.name
		}
		
		this.api.subcatLists(data).subscribe(
			(response: any) => {

                if (JSON.stringify(response.data)) {
					this.loadingSpinner = false;  //    page load spinner 
				}
				this.subCategoriesArray = response.data;
				this.messageNotFound = 'No Record Found';
			},(err: any) => {
				console.log(err)
			}
		)
	}

	selectedFile: any = null;
	onFileSelected(event: any) {
		this.selectedFile = event.target.files[0];
	}
	addSubcategory() {
		// debugger
		this.route.queryParams.subscribe((params: { [x: string]: string | undefined; }) => {
			this.id = params['id'];

		});
		const fd = new FormData();
		fd.append('subCategories_image', this.selectedFile, this.selectedFile.name)
		fd.append('subCategories_name', this.subCategories_name)
		fd.append('subCategories_cat_id', this.id)
		// alert(this.stores_image);
		if ((this.subCategories_name == '' || this.subCategories_name == undefined || this.subCategories_name.length < '3') || (this.selectedFile == '' || this.selectedFile == null)) {

			Swal.fire({
				title: 'Oops! Please Enter Valid details',
				text: 'Thanks!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				cancelButtonText: 'Cancel'
			})
		}
		else {

			this.api.addSubcategory(fd).subscribe(
				(response: any) => {
					Swal.fire({
						title: 'Subcategory Added Successfully',
						text: 'Thanks!',
						icon: 'success',
						showCancelButton: true,
						confirmButtonText: 'Ok',
						cancelButtonText: 'Cancel'
					})
					this.subcatLists();
				}, (err: any) => {
					console.log(err)
				}
			)
		}

	}
	searchApi() {
		if ((this.subcategory_search == '') || (this.subcategory_search == undefined)) {
			this.subcatLists();
			Swal.fire({
				title: 'Oops! Please Enter Sub Category.',
				text: 'Thanks!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				cancelButtonText: 'Cancel'
			})
		} else {

			// debugger
			this.loading = true;


			var data = {};
			data = {
				subcategory_search: this.subcategory_search

			}

			
			//const fd = new FormData();
			//fd.append('products_name',this.products_name) 
			this.api.searchApi(data).subscribe(
				(response: any) => {
					this.loading = false;

					this.subCategoriesArray = response.data;
					//this.storeLists();
				}, (err: any) => {
					this.loading = false;
					console.log(err)
				}
			)
		}
	}

	filterSubCat(){
		
		this.if_records = false;
		var str2 = this.subcategory_search.toLowerCase();
		var arr :any =[];

		this.subCategoriesArray.forEach((element :any)=> {
			var str1 = element.subCategories_name.toLowerCase();
			arr.push(str1);
			if(str1.indexOf(str2) != -1){
				this.if_records = true;
				console.log(str2 + " found");
			}
		
		});
		
	}


	// searchsubCat(){
	//   // debugger
	//   var data = {};
	//    data = {
	//        subcategory_search: this.subcategory_search
	//      }
	//   //const fd = new FormData();
	//   //fd.append('products_name',this.products_name) 
	//   this.api.searchsubCat(data).subscribe(
	//     (response: any) => {
	//       this.subCategoriesArray = response.data;
	//       //this.storeLists();
	//     }, (err: any) => {
	//       console.log(err)
	//     }
	//   )
	// }

	
	deleteSubCategories(subCategories_id: any) {
		var data = {
			subCategories_id: subCategories_id
		}
		var x = confirm("Are you sure you want to delete this store?");
		if (x) {
			this.api.deleteSubCategories(data).subscribe(
				(response: any) => {

					this.subcatLists();
					Swal.fire({
						title: 'Subcategory deleted successfully',
						text: 'Thanks!',
						icon: 'success',
						showCancelButton: true,
						confirmButtonText: 'Ok',
						cancelButtonText: 'Cancel'
					})
				}, (err: any) => {
					console.log(err)
				}

			)
			return true;
		} else {
			return false;
		}

	}
	editSubcats(subCategories_id: any, subCategories_name: any, subCategories_image: any) {
		this.subCategories_id = subCategories_id;
		this.subCategories_name = subCategories_name;
		this.subCategories_image = subCategories_image;

	}
	editSubcategory() {
		const fd = new FormData();
		fd.append('subCategories_image', this.selectedFile, this.selectedFile.name)
		fd.append('subCategories_name', this.subCategories_name)
		fd.append('subCategories_id', this.subCategories_id)
		alert(this.subCategories_id);
		if ((this.subCategories_name == '' || this.subCategories_name == undefined || this.subCategories_name.length < '3') || (this.selectedFile == '' || this.selectedFile == null)) {

			Swal.fire({
				title: 'Oops! Please Enter Valid details',
				text: 'Thanks!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				cancelButtonText: 'Cancel'
			})
		} else {

			this.api.editSubcategory(fd).subscribe(
				(response: any) => {
					Swal.fire({
						title: 'Subcategory Updated Successfully',
						text: 'Thanks!',
						icon: 'success',
						showCancelButton: true,
						confirmButtonText: 'Ok',
						cancelButtonText: 'Cancel'
					})
					this.subcatLists();
				}, (err: any) => {
					console.log(err)
				}
			)
		}
	}
}
