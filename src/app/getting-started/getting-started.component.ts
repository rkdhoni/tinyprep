import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { Router } from '@angular/router';
import { BackendapiService } from '../services/backendapi.service';


@Component({
  selector: 'app-getting-started',
  templateUrl: './getting-started.component.html',
  styleUrls: ['./getting-started.component.css']
})
export class GettingStartedComponent implements OnInit {
  currentPlayingVideo!: HTMLVideoElement;
  name = "Angular";
  @ViewChild("videoPlayer", { static: false })
  videoplayer!: ElementRef;
  isPlay: boolean = false;
  img: any;

  hitTry: any;
 
  
  //  imageDirectoryPlay: any='assets/theme/images/play.png';
  //let  imageDirectoryPause: any='assets/theme/images/pause.png';
  constructor(usernameElement: ElementRef,private api: BackendapiService,private router: Router) {
    var user = localStorage.getItem('token');
    var userType = localStorage.getItem('type');  

    if (user) {                            //user is used to restrict page access 
      // this.router.navigate(['/dashboard']);
    } else {
      // No user is signed in.
      this.router.navigate(['/']);
    }
  } 
 


  ngOnInit(): void {
    // alert("Sdf")
    this.togglePlayPause();

  }
 
  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }
  togglePlayPause(){
    let  imageDirectoryPlay: any='assets/theme/images/play.png';
    let  imageDirectoryPause: any='assets/theme/images/pause.png';

     this.img = imageDirectoryPlay;
  }
  playPause(val:any) {

    var video  =  document.getElementsByClassName('playPauseVideo')
    // console.log(video.length)
    for(var i = 0 ; i < video.length ; i ++ ){
      if(val != i){
        var v= <HTMLMediaElement> video[i];
        // console.log(v)
        v.pause();
        var element = document.getElementById("play"+i)
        // console.log(element)
        if(element != null){
          element.style.display = "block";
        }
        var element = document.getElementById("pause"+i)
        // console.log(element)
        if(element != null){
          element.style.display = "none";
        }
      }
      
    }
    var media = <HTMLMediaElement> document.getElementById("video"+val)
    console.log(media.paused)
    if(media.paused){
      media.play()
      var element = document.getElementById("play"+val)
      // console.log(element)
      if(element != null){
        element.style.display = "none";
      }
      var element = document.getElementById("pause"+val)
      // console.log(element)
      if(element != null){
        element.style.display = "block";
      }
    }else{
      media.pause()
      var element = document.getElementById("play"+val)
      // console.log(element)
      if(element != null){
        element.style.display = "block";
      }
      var element = document.getElementById("pause"+val)
      // console.log(element)
      if(element != null){
        element.style.display = "none";
      }
      // document.getElementById("pause"+val).style.display = "none";
    }
    

  // // debugger
  //   // document.getElementsByClassName('videos');
  //   var myVideoList: any = [];
  //   myVideoList.push(document.getElementById("video0"));
  //   myVideoList.push(document.getElementById("video1"));
  //   myVideoList.push(document.getElementById("video2"));
  //   myVideoList.push(document.getElementById("video3"));
  //   myVideoList.push(document.getElementById("video4"));
  //   myVideoList.push(document.getElementById("video5"));

  

  // for (var i = 0; i < myVideoList.length; i++) {
  //   if(i == val){
  //     this.hitTry =val;
  //    // alert(this.hitTry);
  //     if(myVideoList[i].paused){
  //       myVideoList[i].play();
  //       this.img = 'assets/theme/images/play.png';
  //     }
  //     else{
       
  //       myVideoList[i].pause();
  //       this.img = 'assets/theme/images/play.png';
  //     }
  //   } else{
   
  //     myVideoList[i].pause();
  //     this.img = 'assets/theme/images/pause.png';
  //   }
  // }
}
        
  //   if (myVideo.paused){
  //     myVideo.play();
  //     this.img = imageDirectoryPause;
  //  }  else {
  //       this.img = imageDirectoryPlay;
  //       myVideo.pause();
  //   }    
    //////////////////////////////////////////////////////////////////
  //  let  imageDirectoryPlay: any='assets/theme/images/play.png';
  //  let  imageDirectoryPause: any='assets/theme/images/pause.png';
  //  this.img = imageDirectoryPlay;

  //  var videoList = document.getElementsByTagName("video");
  // alert(videoList);
  
        // for (var i = 0; i < videoList.length; i++) {
        //     videoList[i].pause();
            
        // }
        
  //       if(val == '2'){
       
  //       this.hitTry =val;
  //       // alert(this.hitTry);
       
  //         var myVideo: any = document.getElementById("video2");
        
       
  //       }else if(val == '3'){
  //         this.hitTry =val;
  //         var myVideo: any = document.getElementById("video3");
        
  //       }else if(val == '4'){
  //         this.hitTry =val;
  //         var myVideo: any = document.getElementById("video4");
        
  //       }else if(val == '5'){
  //         this.hitTry =val;
  //         var myVideo: any = document.getElementById("video5");
         
  //       }else if(val == '6'){
  //         this.hitTry =val;
  //         var myVideo: any = document.getElementById("video6");
        
  //       }else if(val == '7'){
  //         this.hitTry =val;
  //         var myVideo: any = document.getElementById("video7");
         
  //       }


  //       if (myVideo.paused){
  //          myVideo.play();
  //          this.img = imageDirectoryPause;
  //       }  else {
  //            this.img = imageDirectoryPlay;
  //            myVideo.pause();
  //        }    
  // }


  

}