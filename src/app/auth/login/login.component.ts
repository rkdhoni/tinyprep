import { Component, OnInit } from '@angular/core';
import { BackendapiService } from 'src/app/services/backendapi.service';
import { Router } from '@angular/router';
import { SocialAuthService, GoogleLoginProvider, SocialUser, FacebookLoginProvider } from 'angularx-social-login';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import Swal from 'sweetalert2';
import * as $ from 'jquery';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  // providers: [AuthService]
  
})
export class LoginComponent implements OnInit {
  //socialPlatformProvider:any |undefined;
  email: string = "";
  password: string = "";
  messageError:any;
  show: boolean = false;
  socialUser:any = SocialUser;
  isLoggedin: boolean | undefined;
  formGroup= new FormGroup({});
  loading: boolean = false;
  userDataArray: any;
  name: any;
  defaultimage: any="http://i.pravatar.cc/500?img=7";
  imageDirectoryPath: any = 'https://enacteservices.net/tinyprepbackend/';
  // emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  remember_me:any = ""; 
  
  constructor(private api: BackendapiService,private router: Router,private socialAuthService: SocialAuthService ,private _fb: FormBuilder ) {

    var user = localStorage.getItem('token');
    var userType= localStorage.getItem('type');

    if (user){          
      if(userType=="1"){
        this.router.navigate(['/categories']);
      }else{
        this.router.navigate(['/dashboard'])
      }                
    }



  }

  ngOnInit(): void {

    this.remember_me = localStorage.getItem("remember_me");

    this.formGroup = this._fb.group({
      //  'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      // 'old_password': ['',[Validators.required]],
       'email': ['',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
       'password': ['',[Validators.required]]
    })

    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      console.log(this.socialUser);
    });


    if(this.remember_me == "true"){
      this.remember_me = true;
      
    }else{
     this.remember_me = false;
    }
  }
 
  loginWithGoogle(): void {
    
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      // alert(JSON.stringify(this.socialUser));
      let data = {                       
        name: this.socialUser['name'],
        social_id: this.socialUser['id'],
        email:this.socialUser['email'],
        provider:this.socialUser['provider'], 
        image:this.socialUser['photoUrl']
      }
      this.api.socialLogin(data).subscribe(
        (response: any) => {
          var res = response;
          // this.router.navigate(['dashboard'])
        if(res.error == 1){
          this.messageError = res.message;
        }else{
          this.messageError = "";
         
          localStorage.setItem("token",res.data['social_id'])
          localStorage.setItem("type",'2')
          this.router.navigate(['/tinyprep'])
          this.router.navigate(['dashboard'])
        }
        }, (err: any) => {
               console.log(err)
        }
      )
    });
  
  }



//   function rememberMe() {
//     $cookie = isset($_COOKIE['rememberme']) ? $_COOKIE['rememberme'] : '';
//     if ($cookie) {
//         list ($user, $token, $mac) = explode(':', $cookie);
//         if (!hash_equals(hash_hmac('sha256', $user . ':' . $token, SECRET_KEY), $mac)) {
//             return false;
//         }
//         $usertoken = fetchTokenByUserName($user);
//         if (hash_equals($usertoken, $token)) {
//             logUserIn($user);
//         }
//     }
// }

// function onLogin($user) {
//   $token = GenerateRandomToken(); // generate a token, should be 128 - 256 bit
//   storeTokenForUser($user, $token);
//   $cookie = $user . ':' . $token;
//   $mac = hash_hmac('sha256', $cookie, SECRET_KEY);
//   $cookie .= ':' . $mac;
//   setcookie('rememberme', $cookie);
// }

  loginWithFacebook(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      let data = {                       
        name: this.socialUser['name'],
        social_id: this.socialUser['id'],
        email:this.socialUser['email'],
        provider:this.socialUser['provider'], 
        image:this.socialUser['photoUrl']
      }
      this.api.socialLogin(data).subscribe(
        (response: any) => {
          var res = response;
          // this.router.navigate(['dashboard'])
        if(res.error == 1){
          this.messageError = res.message;
        }else{
          this.messageError = "";
          localStorage.setItem("token",res.data['social_id'])
          localStorage.setItem("type",'2')
          this.router.navigate(['/tinyprep'])
          this.router.navigate(['dashboard'])
        }
        }, (err: any) => {
               console.log(err)
        }
      )
    });
  
  }
  password_show() {
    this.show = !this.show;
   }
  submitLoginForm() {

    this.loading = true;
    
    let data = {                        //let temporary variable  //data object
      email: this.email,
      password: this.password,
    }

    if ((this.password == '' || this.password == undefined )
    || (this.email == '' || this.email == undefined || this.email.length < 6)){
      this.loading = false;
      Swal.fire({
        title: 'Oops! Please Enter Valid details',
        text: 'Thanks!',
        icon: 'warning',
        confirmButtonText: 'Ok',
      })   
    }else{

    this.api.login(data).subscribe(
      (response :any) => {
        this.loading = false;
        var res = response;
        if(res.error == 1){
          Swal.fire({
            title: 'Oops! Please Enter Valid details',
            text: 'Thanks!',
            icon: 'warning',
            confirmButtonText: 'Ok',
          }) 
          this.messageError = res.message;
        }else{
          this.loading = false;
          this.messageError = "";

          if(res.data['type'] == 1 ){
            localStorage.setItem("type",res.data['type'])
            localStorage.setItem("token",res.data['token'])
            this.router.navigate(['categories'])
          }else{
            localStorage.setItem("type",res.data['type'])
            localStorage.setItem("token",res.data['token'])
            this.router.navigate(['dashboard'])
          }
          this.userData();
        }   
      },err => {
        this.loading = false;
        Swal.fire({
          title: 'Oops! Please Enter Valid details',
          text: 'Thanks!',
          icon: 'warning',
          confirmButtonText: 'Ok',
        }) 
        console.log(err)
      }
    )
   }
  }

  applelogin(){
    $('.underdevelopment-go-action').trigger('click');
  }
   
  
  userData() {
    
    this.api.userData().subscribe(
      (response: any) => {
        var $this = this;
        let userDataArray = response.data[0];
        
        if(userDataArray){
          $('.header-name-right').show();
          $('.header-name-right #cuserid').val((userDataArray?userDataArray.id:''))
          $('.header-name-right .profile-images img').attr('src',(userDataArray?$this.imageDirectoryPath+userDataArray.image:$this.defaultimage));
          $('.header-name-right .cusername td').text((userDataArray?userDataArray.name:'')); 
        }
      }, (err: any) => {
        console.log(err)
      }
    )
  }

  remembercheckCheckBoxvalue(event:any){

    alert('hello');

    this.remember_me = event.target.checked;
  }
}  
