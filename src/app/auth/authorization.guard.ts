// import { Injectable } from '@angular/core';
// import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { Observable } from 'rxjs/Observable';

// import { AppContextService } from './context';

// @Injectable()
// export class AuthorizationGuard implements CanActivate {
//     constructor(
//         private appContextService: AppContextService
//     ) { }
//     canActivate(
//         next: ActivatedRouteSnapshot,
//         state: RouterStateSnapshot): boolean {
//         return this.appContextService.getAuthAdminLoggednIn();
//     }
// }