import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackendapiService {
  // filteredProduct: any;
 // subcatLists: any;
  
  // searchProducts(data: {}) {
  //   throw new Error('Method not implemented.');
  // }
  categoryLists() {
    throw new Error('Method not implemented.');
  }


  rootUrl: string = environment.base_url;
  constructor(private http: HttpClient, private router: Router) { }

  login(data: any) {
    // this.rootUrl = environment.baseUrl;
    // let headers = new HttpHeaders({
    // 'Content-Type': 'application/json',
    // 'Authorization': 'Bearer '+api.api_token });
    // let options = { headers: headers };
    return this.http.post(this.rootUrl + 'login', data);
  }

  categoriesListing() {

    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'categoriesListing', null, options);
  }  
  
  allProductsLists() {

    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'allProductsLists', null, options);
  } 
  productsListing(data: any) {

    // let headers = new HttpHeaders({ //let is used to create temporary
    // 'Content-Type': 'application/json',
    // 'Authorization': 'Bearer '+localStorage.getItem('token') });
    // let options = { headers: headers };
    return this.http.post(this.rootUrl + 'productsListing', data);
  } 
 
  addProducts(data: any) {
    let headers = new HttpHeaders({ //let is used to create temporary variable
      "Accept": "multipart/form-data",
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    }); 
    //let auth_id = auth()->id;         
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'addProducts', data, options);
  }
  
  addUsers(data: any) {
    //console.log(data);  
    return this.http.post(this.rootUrl + 'addUsers', data);
  }
  addCategory(data: any) {
    let headers = new HttpHeaders({ //let is used to create temporary variable
      "Accept": "multipart/form-data",
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    }); 
    //let auth_id = auth()->id;         
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'addCategory', data, options);
  }    
  addStore(data: any) {    
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'addStore', data, options);
  } 
  deleteCategory(data: any) {
    return this.http.post(this.rootUrl + 'deleteCategory', data);
  }
  addToFavorite(data: any) {
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'addToFavorite', data, options);
  }

  editProducts(data: any) {
    return this.http.post(this.rootUrl + 'editProducts', data);
  } 
  
  viewProduct(data: any) {

    return this.http.post(this.rootUrl + 'viewProduct', data);

  } 

  deleteProduct(data: any) {
    return this.http.post(this.rootUrl + 'deleteProduct', data);
  }
  deleteCategoryLists(data: any) {
    return this.http.post(this.rootUrl + 'deleteCategoryLists', data);
  }

  shoppingLists() {
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'shoppingLists', null, options);
  }  
  favoriteLists() {
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'favoriteLists', null, options);
  }
  userLists() {
    return this.http.post(this.rootUrl + 'userLists', null);
  } 
  
  editUsers(data: any) {
    return this.http.post(this.rootUrl + 'editUsers', data);
  }
  deleteUser(data: any) {
    return this.http.post(this.rootUrl + 'deleteUser', data);
  } 
  storeLists() {
    let headers = new HttpHeaders({ 
      "Accept": "multipart/form-data",
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'storeLists', null, options);
  } 
  
  addStores(data: any) {
    //console.log(data);  
    let headers = new HttpHeaders({ 
      "Accept": "multipart/form-data",
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'addStores', data, options);
  }  
  
  deleteStore(data: any) {
    return this.http.post(this.rootUrl + 'deleteStore', data);
  }   
  
  editStores(data: any) {
    return this.http.post(this.rootUrl + 'editStores', data);
  } 
  // defaultStoreLists() {
  //   return this.http.post(this.rootUrl + 'defaultStoreLists', null);
  // } 
  productsLists(data:any) {

    let headers = new HttpHeaders({ //let is used to create temporary
      
    'Authorization': 'Bearer '+localStorage.getItem('token') });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'productsLists',  options);
  }  
  
  editCategories(data: any) {
    let headers = new HttpHeaders({ //let is used to create temporary
      "Accept": "multipart/form-data",
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
    return this.http.post(this.rootUrl + 'editCategories', data, options);
  } 
  searchApi(data: any) {
    return this.http.post(this.rootUrl + 'searchApi', data);
  } 
 
  categories() {
    return this.http.post(this.rootUrl + 'categories', null);
  } 

  subcategories(){
    return this.http.post(this.rootUrl + 'subcategories', null);
  }

  searchCatStore(data: any) {
    return this.http.post(this.rootUrl + 'searchCatStore', data);
  }    
  searchProducts(data: any) {
    return this.http.post(this.rootUrl + 'searchProducts', data);
  } 
  searchProducts_filter(data: any) {
    return this.http.post(this.rootUrl + 'searchProducts_filter', data);
  } 
  subcatLists(data: any) {
    return this.http.post(this.rootUrl + 'subcatLists', data);
  } 
  addSubcategory(data: any) {
   
    return this.http.post(this.rootUrl + 'addSubcategory', data);
  } 
  deleteSubCategories(data: any) {
    return this.http.post(this.rootUrl + 'deleteSubCategories', data);
  }
  editSubcategory(data: any) {
    return this.http.post(this.rootUrl + 'editSubcategory', data);
  }  
  subcategoryLists(data: any) {
    return this.http.post(this.rootUrl + 'subcategoryLists', data);
  }  

  filteredProductLists(data:any) {

    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'filteredProductLists', data, options);
  }  
  
  // userData() {

  //     let headers = new HttpHeaders({ 
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer ' + localStorage.getItem('token')
  //     });
  //     let options = { headers: headers };
  //     return this.http.post(this.rootUrl + 'userData', options);
  //   }
    userData() {
      let headers = new HttpHeaders({ //let is used to create temporary
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      });
      let options = { headers: headers };
      return this.http.post(this.rootUrl + 'userData', null, options);
    }

  socialLogin(data: any) {
    return this.http.post(this.rootUrl + 'socialLogin', data);
  }  
  changePassword(data:any) {

    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'changePassword', data, options);
  }   
  
 
  subcatListing() {
    return this.http.post(this.rootUrl + 'subcatListing', null);  
  } 
  deleteSubCat(data: any) {
    return this.http.post(this.rootUrl + 'deleteSubCat', data);
  }  
  forgotPassword(data: any) {
    return this.http.post(this.rootUrl + 'forgotPassword', data);
  }
  resetPassword(data: any) {
    return this.http.post(this.rootUrl + 'resetPassword', data);
  }
  mystoreLists() {
     let headers = new HttpHeaders({ //let is used to create temporary
    'Authorization': 'Bearer '+localStorage.getItem('token') });
     let options = { headers: headers };
     return this.http.post(this.rootUrl + 'mystoreLists',  options);
  }
  filteredProduct(data: any) {
    return this.http.post(this.rootUrl + 'filteredProduct', data);
  }
  // searchsubCat(data: any) {
  //   return this.http.post(this.rootUrl + 'searchsubCat', data);
  // }  
  editProfile(data:any) {

    let headers = new HttpHeaders({ 
      "Accept": "multipart/form-data",
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'editProfile', data, options);
  } 
  defStore() {

    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'defStore', null, options);
  } 
  favStore() {

    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'favStore', null, options);
  }

  shopList(data:any) {

    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'shopList', data, options);
  }

  shopFilterList(data:any){
    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'shopFilterList', data, options);
  }
  editItems(data:any){
    return this.http.post(this.rootUrl + 'editItems', data);
  }



  searchProductsfrompopup(data:any){
    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.get(this.rootUrl + 'searchProductsfrompopup?'+data,options);

  }

  getQuizitemdata(){
    let headers = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getQuizitemdata',null, options);
  }
}
