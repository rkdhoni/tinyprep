import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import Swal from "sweetalert2";
import { BackendapiService } from "../services/backendapi.service";
import {FormGroup,FormControl,Validators,FormBuilder,AbstractControl} from "@angular/forms";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-stores",
  templateUrl: "./stores.component.html",
  styleUrls: ["./stores.component.css"],
})
export class StoresComponent implements OnInit {
  storeListsArray: any;
  stores_image: any;
  stores_name: any;
  stores_link: any;
  messageError: any;
  imageDirectoryPath: any = "https://enacteservices.net/tinyprepbackend/";
  stores_id: any;
  imageSrc: string | undefined;
  myForm: any;
  searchName: any;
  storesearchListsArray: any;
  formGroup = new FormGroup({});
  loading: boolean = false;
  messageNotFound: any;
  disablebtn:any = false;

  isSubmitted = false;

  // urlRegEx =
  // '[-a-zA-Z0-9@:%_+.~#?&//=]{2,256}(.[a-z]{2,4})?\b(/[-a-zA-Z0-9@:%_+.~#?&//=]*)?';  

  urlRegEx = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
  @ViewChild('myModal_edit_btn_close')
  myModal_edit_btn_close!: ElementRef;


  @ViewChild('myModal_add_btn_close')
  myModal_add_btn_close!: ElementRef;
  formvalid: any;

  

  constructor(
    private api: BackendapiService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private _fb: FormBuilder
  ) {
    this.storeLists();
  }

  ngOnInit(): void {

    this.formGroup = this._fb.group({
      stores_name: ["", [Validators.required]],
      stores_image: ["", [Validators.required]],
      stores_link: ["",[Validators.required, Validators.pattern(this.urlRegEx)],
      ],
    });

  }

  validateAllFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFields(control);
      }
    });
  }

  // get f(): { [key: string]: AbstractControl } {
  //   return this.formGroup.controls;
  // }

  // onSubmit(): void {
  //   this.isSubmitted = true;

  //   if (this.formGroup.invalid) {
  //     return;
  //   }

  //   console.log(JSON.stringify(this.formGroup.value, null, 2));
  // }

 
  storeLists() {

    this.api.storeLists().subscribe(
      (response: any) => {
        this.messageNotFound = "No Record Found";
        this.storeListsArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  selectedFile: any = null;
  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];

    console.log(this.selectedFile);
  }
  searchApi() {
   
    this.loading = true;
    var data = {};
    data = {
      stores_name: this.searchName,
    };

    this.api.searchApi(data).subscribe(
      (response: any) => {
        this.loading = false;
        this.messageNotFound = "No Record Found";
        this.storeListsArray = response.data;
        //this.storeLists();
      },
      (err: any) => {
        this.loading = false;
        console.log(err);
      }
    );
  }


  // addStores(){

  //   this.isSubmitted = true;

  //   if (this.formGroup.invalid) {
  //     return;
  //   }

  //   console.log(JSON.stringify(this.formGroup.value, null, 2));

  // }

  addStores() {
    this.validateAllFields(this.formGroup); 
    //this.loading = true;
    const fd = new FormData();
    fd.append("stores_image", this.selectedFile, this.selectedFile.name);
    fd.append("stores_name", this.stores_name);
    fd.append("stores_link", this.stores_link);

    if(
      this.stores_name == "" ||
      this.stores_name == undefined ||
      this.stores_link == "" ||
      this.stores_link == undefined
    ) {
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        text: "Thanks!",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    } else {
      this.api.addStores(fd).subscribe(
        (response: any) => {
          var res = response;
          if (res.error == 1) {
            Swal.fire({
              title: "Oops! Please Enter Valid details",
              text: "Thanks!",
              icon: "warning",
              confirmButtonText: "Ok",
            });
            this.messageError = res.message;
          } else {

            this.loading = false;
            this.myModal_add_btn_close.nativeElement.click();
            this.messageError = "";
            Swal.fire({
              title: "Store Added Successfully",
              text: "Thanks!",
              icon: "success",
              confirmButtonText: "Ok",
            });

            this.storeLists();
          }
        },
        (err: any) => {
          this.loading = false;

          Swal.fire({
            title: "Oops! Please Enter Valid details",
            text: "Thanks!",
            icon: "warning",
            confirmButtonText: "Ok",
          });
          console.log(err);
        }
      );
    }
  }

  editStore(
    stores_id: any,
    stores_name: any,
    stores_link: any,
    stores_image: any
  ) {
    this.stores_id = stores_id;
    this.stores_name = stores_name;
    this.stores_link = stores_link;
    this.stores_image = stores_image;
  }
  editStores() {
    
    this.loading = true;
    const fd = new FormData();

    // if (this.selectedFile.name) {
    //   fd.append("stores_image", this.selectedFile, this.selectedFile.name);
    // } else {
    //   fd.append("stores_image", this.stores_image);
    // }

    fd.append("stores_image", this.selectedFile, this.selectedFile.name);
    fd.append("stores_name", this.stores_name);
    fd.append("stores_link", this.stores_link);
    fd.append("stores_id", this.stores_id);

    if (
      this.stores_name == "" ||
      this.stores_name == undefined ||
      this.stores_name.length < "3" ||
      this.stores_link == "" ||
      this.stores_link == undefined ||
      this.stores_link.length < "5" 
      
    ) {
      this.loading = false;
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        text: "Thanks!",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    } else {

      this.api.editStores(fd).subscribe(
        (response: any) => {
          console.log(response);
          
          var res = response;
          if (res.error == 1) {
            this.loading = false;
            this.myModal_edit_btn_close.nativeElement.click();
            Swal.fire({
              title: "Oops! Please Enter Valid details",
              text: "Thanks!",
              icon: "warning",
              confirmButtonText: "Ok",
              
            });
            this.messageError = res.message;
          } else {
            this.messageError = "";
            this.storeLists();
            this.loading = false;
            this.myModal_edit_btn_close.nativeElement.click();
            Swal.fire({
              title: "Store Updated Successfully",
              text: "Thanks!",
              icon: "success",
              confirmButtonText: "Ok",
            });
          }
        },
        (err: any) => {

          console.log(err);
          this.loading = false;
          this.myModal_edit_btn_close.nativeElement.click();
          Swal.fire({
            title: "Oops! Please Enter Valid details",
            text: "Thanks!",
            icon: "warning",
            confirmButtonText: "Ok",
          });
        }
      );
    }
  }

  deleteStore(stores_id: any) {
    var data = {
      stores_id: stores_id,
    };
    var x = confirm("Are you sure you want to delete this store?");
    if (x) {
      this.api.deleteStore(data).subscribe(
        (response: any) => {

          Swal.fire({
            title: "Store deleted Successfully",
            text: "Thanks!",
            icon: "success",
            confirmButtonText: "Ok",
          });
          this.storeLists();
        },
        (err: any) => {
          console.log(err);
        }
      );
      return true;
    } else {
      return false;
    }
  }

}
