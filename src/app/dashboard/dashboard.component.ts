import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendapiService } from 'src/app/services/backendapi.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userDataArray: any;
  currentDate: Date;
  // router: any;

  constructor(private api: BackendapiService,private router: Router) {
    this.currentDate = new Date();


    var user = localStorage.getItem('token');
      // var userType = localStorage.getItem('type');  

      if (!user) {                            //user is used to restrict page access 
        this.router.navigate(['/']);
      } 
  
  }
  

  ngOnInit(): void {
    this.userData();
  }
  

  //  playPause() { 
  //   let myVideo = document.getElementById("video1"); 
  //     alert(myVideo);
  //   // if (myVideo.paused) {
  //   //   myVideo.play(); 
  //   // }
    
  //   // else {
  //   //   myVideo.pause(); 
  //   // }
     
  //   } 
  userData() {
    this.api.userData().subscribe(
      (response: any) => {
         this.userDataArray = response.data;
      }, (err: any) => {
        console.log(err)
      }
    )
  }
}
