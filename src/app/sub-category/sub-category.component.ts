import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import Swal from "sweetalert2";
import { BackendapiService } from "../services/backendapi.service";
import { HttpClient } from "@angular/common/http";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";

@Component({
  selector: "app-sub-category",
  templateUrl: "./sub-category.component.html",
  styleUrls: ["./sub-category.component.css"],
})
export class SubCategoryComponent implements OnInit {
  imageDirectoryPath: any = "https://enacteservices.net/tinyprepbackend/";
  subArray: any;
  ShowSubmitLoader: boolean = false;
  formGroup = new FormGroup({});
  activeCategoryId: any;
  categoriesArray: any;
  selectElementText: any;
  subCategories_name: any;
  subCategories_image: any;
  products_cat_id: any;
  id: any;
  name: any;
  subCategories_id: any;
  loading: boolean = false;
  messageError: any;
  messageNotFound: any;
  subcategory_search: any;

  @ViewChild("myModal_edit_btn_close")
  myModal_edit_btn_close!: ElementRef;

  @ViewChild("myModal_edit_btn_close2")
  myModal_edit_btn_close2!: ElementRef;

  constructor(
    private api: BackendapiService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private _fb: FormBuilder
  ) {
    this.subcatListing();
    this.categories();
  }

  ngOnInit(): void {
    this.formGroup = this._fb.group({
      //  'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      name: ["", [Validators.required, Validators.minLength(3)]],
      subCategories_image: ["", [Validators.required]],
      // 'products_cat_id': ['', [Validators.required]]
    });
  }

  subcatListing() {
    this.api.subcatListing().subscribe(
      (response: any) => {
        this.subArray = response.data;
        // console.log(response);
      },
      (err: any) => {
        console.log(err);
      }
    );
  }
  searchApi() {
    // debugger
    this.loading = true;
    var data = {};
    data = {
      subcategory_search: this.subcategory_search,
    };

    this.api.searchApi(data).subscribe(
      (response: any) => {
        this.loading = false;
        this.categories();
        this.messageNotFound = "No Record Found";
        this.subArray = response.data;
        //this.storeLists();
      },
      (err: any) => {
        this.loading = false;
        console.log(err);
      }
    );
  }

  categories() {
    this.api.categories().subscribe(
      (response: any) => {
        this.categoriesArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  getSelectedOptionText(event: any) {
    let selectedOptions = event.target["options"];
    let selectedIndex = selectedOptions.selectedIndex;
    this.selectElementText = selectedOptions[selectedIndex].value;
    // alert(this.selectElementText);
    // console.log(selectElementText)
  }
  selectedFile: any = null;
  onFileSelected(event: any) {
    ///let fileList: FileList = event.target.files;
    // let file: File = fileList[0];
    // this.fd.append('uploadFile', file, file.name);
    this.selectedFile = event.target.files[0];
  }
  addSubcategory() {
    this.loading = true;
    const fd = new FormData();
    fd.append("subCategories_image", this.selectedFile, this.selectedFile.name);
    fd.append("subCategories_name", this.name);
    fd.append("subCategories_cat_id", this.selectElementText);

    if (
      this.name == "" ||
      this.name == undefined ||
      this.name.length < "3" ||
      this.selectedFile == "" ||
      this.selectedFile == null
    ) {
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        text: "Thanks!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Ok",
        cancelButtonText: "Cancel",
      });
    } else {
      this.api.addSubcategory(fd).subscribe(
        (response: any) => {
          var res = response;
          if (res.error == 1) {
            this.loading = false;
            this.myModal_edit_btn_close.nativeElement.click();
            this.messageError = res.message;

            Swal.fire({
              title: "Oops! Please Enter Valid details",
              text: this.messageError,
              icon: "warning",
              showCancelButton: true,
              confirmButtonText: "Ok",
              cancelButtonText: "Cancel",
            });
          } else {
            this.messageError = "";
            Swal.fire({
              title: "Subcategory Added Successfully",
              text: "Thanks!",
              icon: "success",
              showCancelButton: true,
              confirmButtonText: "Ok",
              cancelButtonText: "Cancel",
            });
            this.loading = false;
            this.myModal_edit_btn_close.nativeElement.click();

            this.subcatListing();
          }
        },
        (err: any) => {
          this.loading = false;
          this.myModal_edit_btn_close.nativeElement.click();

          console.log(err);
        }
      );
    }
  }

  editSubcats(
    subCategories_id: any,
    name: any,
    subCategories_name: any,
    subCategories_image: any
  ) {
    //editSubcats(post.subCategories_id,post.subCategories_name,post.subCategories_image)"
    this.subCategories_id = subCategories_id;
    this.name = name;
    this.subCategories_name = subCategories_name;
    this.subCategories_image = subCategories_image;
  }
  editSubcategory() {
    this.loading = true;
    const fd = new FormData();
    fd.append("subCategories_image", this.selectedFile, this.selectedFile.name);
    fd.append("subCategories_name", this.subCategories_name);
    fd.append("subCategories_id", this.subCategories_id);
    // if ((this.subCategories_name == '' || this.subCategories_name == undefined || this.subCategories_name.length < '3' ) ||  (this.selectedFile == '' || this.selectedFile == null )){

    //   Swal.fire({
    //     title: 'Oops! Please Enter Valid details',
    //     text: 'Thanks!',
    //     icon: 'warning',
    //     showCancelButton: true,
    //     confirmButtonText: 'Ok',
    //     cancelButtonText: 'Cancel'
    //   })
    // }else {

    this.api.editSubcategory(fd).subscribe(
      (response: any) => {
        console.log(response);

        var res = response;
        if (res.error == 1) {
          this.loading = false;
          this.myModal_edit_btn_close2.nativeElement.click();
          this.messageError = res.message;
          Swal.fire({
            title: "Oops! Please Enter Valid details",
            text: this.messageError,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
          });
        } else {
          this.messageError = "";
          this.loading = false;
          this.myModal_edit_btn_close2.nativeElement.click();
          this.subcatListing();
          Swal.fire({
            title: "Subcategory Updated Successfully",
            text: "Thanks!!",
            icon: "success",
            showCancelButton: true,
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
          });
        }
      },
      (err: any) => {
        console.log(err);
        this.loading = false;
        this.myModal_edit_btn_close2.nativeElement.click();

        Swal.fire({
          title: "Oops! Please Enter Valid details",
          text: this.messageError,
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Ok",
          cancelButtonText: "Cancel",
        });
        console.log(err);
      }
    );
    // }
  }

  deleteSubCat(id: any) {
    var data = {
      id: id,
    };
    var x = confirm("Are you sure you want to delete?");
    if (x) {
      this.api.deleteSubCat(data).subscribe(
        (response: any) => {
          this.subcatListing();
          Swal.fire({
            title: "Subcategory Deleted Successfully",
            text: "Thanks!",
            icon: "success",
            showCancelButton: true,
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
          });
          this.ShowSubmitLoader = false;
          this.activeCategoryId = "";
        },
        (err: any) => {
          this.activeCategoryId = "";
          this.ShowSubmitLoader = false;
          Swal.fire({
            title: "Oops!Something went wrong",
            text: "Thanks!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
          });
          console.log(err);
        }
      );
      return true;
    } else {
      return false;
    }
  }
  Logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("type");
    this.router.navigate(["/"]);
    // this.router.navigate[('login')]
  }
}
