import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendapiService } from '../services/backendapi.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  new_password: any;
  confirm_password: any;
  resetPasswordArray: any;
  email: string | undefined;
  loading: boolean = false;
  formGroup= new FormGroup({});
  uid: string | undefined;
  constructor(private api: BackendapiService, private router: Router, private http: HttpClient, private route: ActivatedRoute,private _fb: FormBuilder) {
  
    var user = localStorage.getItem('token');
    var userType= localStorage.getItem('type');

    if(user){          
      if(userType=="1"){
        this.router.navigate(['/categories']);
      }else{
        this.router.navigate(['/dashboard'])
      }                 
    } 
    
    this.route.queryParams.subscribe((params: { [x: string]: string | undefined; }) => {
      this.uid = params['uid'];
     
    });
   }

  ngOnInit(): void {
    this.formGroup = this._fb.group({
      'new_password': ['',[Validators.required,Validators.minLength(6)]],
      'confirm_password': ['',[Validators.required,Validators.minLength(6)]]
   })
  }
  resetPassword() {
    this.loading = true;
    this.route.queryParams.subscribe((params: { [x: string]: string | undefined; }) => {
      this.uid = params['uid'];
     
    });
   
    var data = {};
    data = {
      uid:this.uid,
      new_password: this.new_password,
      confirm_password: this.confirm_password
    }

    console.log(data);

    //const fd = new FormData();
    //fd.append('products_name',this.products_name) 
    if((this.new_password == '' || this.new_password == undefined || this.new_password.length < '6' ) || (this.confirm_password == '' || this.confirm_password == undefined ||this.confirm_password.length < '6')){
      Swal.fire({
        title: 'Oops! Please Enter Valid details',
        text: 'Thanks!',
        icon: 'warning',
        //showCancelButton: true,
        confirmButtonText: 'Ok',
       // cancelButtonText: 'Cancel'
      })
    }else{
    this.api.resetPassword(data).subscribe(
      (response: any) => {
        this.loading = false;
        this.resetPasswordArray = response.data;
        Swal.fire({
          title: 'Password Changed successfully',
          text: 'Thanks!',
          icon: 'success',
          //showCancelButton: true,
          confirmButtonText: 'Ok',
          //cancelButtonText: 'Cancel'
        })

        this.router.navigate(['/']);
        
      }, (err: any) => {
        this.loading = false;
        Swal.fire({
          title: 'Oops something went wrong.',
          text: 'Thanks!',
          icon: 'warning',
          //showCancelButton: true,
          confirmButtonText: 'Ok',
          ///cancelButtonText: 'Cancel'
        })
        console.log(err)
      }
    )
    }
  }
}
