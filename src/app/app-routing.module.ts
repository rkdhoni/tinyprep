import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { CategoriesComponent } from './categories/categories.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemsComponent } from './items/items.component';
import { ListComponent } from './list/list.component';
import { ProductsComponent } from './products/products.component';
import { SignupComponent } from './signup/signup.component';
import { ShoppingListsComponent } from './shopping-lists/shopping-lists.component';
import { HeaderComponent } from './header/header.component';
import { UsersComponent } from './users/users.component';
import { StoresComponent } from './stores/stores.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { GetHelpComponent } from './get-help/get-help.component';
import { SubCategoriesComponent } from './sub-categories/sub-categories.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { TinyPrepCourseComponent } from './tiny-prep-course/tiny-prep-course.component';
import { MyStoreComponent } from './my-store/my-store.component';
import { ProfileComponent } from './profile/profile.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'category-list', component: ItemsComponent },
  { path: 'list', component: ListComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'shopping-lists', component: ShoppingListComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'users', component: UsersComponent }, 
  { path: 'stores', component: StoresComponent },
  { path: 'items-list', component: ProductListComponent },
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'get-help', component: GetHelpComponent }, 
  { path: 'sub-category', component: SubCategoriesComponent },
  { path: 'footer', component: FooterComponent },  
  { path: 'sidebar', component: SidebarComponent }, 
  { path: 'change-password', component: ChangePasswordComponent }, 
  { path: 'subcategory', component: SubCategoryComponent }, 
  { path: 'getting-started', component: GettingStartedComponent },  
  { path: 'reset-password', component: ResetPasswordComponent }, 
  { path: 'tinyprep-course', component: TinyPrepCourseComponent }, 
  { path: 'my-store', component: MyStoreComponent }, 
  { path: 'profile', component: ProfileComponent }, 
  { path: 'terms-conditions', component: TermsConditionsComponent },  
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
];

@NgModule({  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
