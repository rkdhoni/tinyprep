import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriesComponent } from './categories/categories.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemsComponent } from './items/items.component';
import { ProductsComponent } from './products/products.component';
import { ListComponent } from './list/list.component';
import { SignupComponent } from './signup/signup.component';
import { ShoppingListsComponent } from './shopping-lists/shopping-lists.component';
import { HeaderComponent } from './header/header.component';
import { UsersComponent } from './users/users.component';
import { StoresComponent } from './stores/stores.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { HttpClientModule } from '@angular/common/http';
import { GetHelpComponent } from './get-help/get-help.component';
import {AccordionModule} from 'primeng/accordion';     
import {MenuItem} from 'primeng/api';

import { Routes, RouterModule } from '@angular/router';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DragDropModule} from 'primeng/dragdrop';
import { TableModule } from 'primeng/table';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { SubCategoriesComponent } from './sub-categories/sub-categories.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
// import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
// import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';
// import { LocationStrategy, PathLocationStrategy } from '@angular/common';
// import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';  
// import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login'; 
import { LocationStrategy, PathLocationStrategy,HashLocationStrategy } from '@angular/common';

import { SocialLoginModule, SocialAuthServiceConfig, FacebookLoginProvider } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { TinyPrepCourseComponent } from './tiny-prep-course/tiny-prep-course.component';
import { MyStoreComponent } from './my-store/my-store.component';
import { ProfileComponent } from './profile/profile.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';



// export function socialConfigs() {
	// const config = new AuthServiceConfig(
	// 	[
	// 		{
	// 			id: FacebookLoginProvider.PROVIDER_ID,
	// 			provider: new FacebookLoginProvider('')
	// 		},
	// 		{
	// 			id: GoogleLoginProvider.PROVIDER_ID,
	// 			provider: new GoogleLoginProvider('')
	// 		}
	// 	]
	// );
	// export function provideConfig() {
  //   return config;
  // }
// }

// const routes: Routes = [

// ];




@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    CategoriesComponent,
    DashboardComponent,
    ItemsComponent,
    ProductsComponent,
    ListComponent,
    SignupComponent,
    ShoppingListsComponent,
    HeaderComponent,
    UsersComponent,
    StoresComponent,
    ProductListComponent,
    ForgetPasswordComponent,
    GetHelpComponent,
    ShoppingListComponent,
    SubCategoriesComponent,
    FooterComponent,
    SidebarComponent,
    ChangePasswordComponent,
    SubCategoryComponent,
    GettingStartedComponent,
    ResetPasswordComponent,
    TinyPrepCourseComponent,
    MyStoreComponent,
    ProfileComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DragDropModule,
    TableModule,
    SocialLoginModule,

   // RouterModule.forRoot(routes, { useHash: true }) 
    
  ],
  providers: [
    // AuthService,
    
      {
        provide: 'SocialAuthServiceConfig',
        useValue: {
          autoLogin: false,
          providers: [
            {
              id: GoogleLoginProvider.PROVIDER_ID,
              provider: new GoogleLoginProvider(
                '771826418030-v9ud1i2gvjajoklv473e49fv2vqasnts.apps.googleusercontent.com'
              )
            },
            {
              id: FacebookLoginProvider.PROVIDER_ID,
              provider: new FacebookLoginProvider('541452427269957')
            }
          ]
        } as SocialAuthServiceConfig,
      },
    
		 { provide: LocationStrategy, useClass: HashLocationStrategy },
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
