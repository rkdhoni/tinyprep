import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BackendapiService } from '../services/backendapi.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  categoresArray: any = [];
  monthsArray: any = [1, 3, 6, 12];
  loadingSpinner: boolean = false;
  availableProducts: any = [
    {
      name: 'Angular',
      category: 'Web Development',
      price: '100',
      id: 1
    },
    {
      name: 'Flexbox',
      category: 'Web Development',
      price: '100',
      id: 2
    },
    {
      name: 'iOS',
      category: 'App Development',
      price: '100',
      id: 3
    },
    {
      name: 'Java',
      category: 'Software development',
      price: '100',
      id: 4
    }
  ];
  avaliabletill: any;
  messageNotFound: any;
  storeListsArray: any;
  shopListArray: any;
  product_storeArray: any;
  products_quantity: any;
  quickinput: any;
  quizitemproductsArray: any = [];
  loading: boolean = false;

  selectedProducts: any = [];

  draggedProduct: any;

  constructor(private api: BackendapiService, private router: Router, private route: ActivatedRoute, private http: HttpClient,
    private _fb: FormBuilder) {

    this.storeLists();
    this.categorieslist();

  }

  ngOnInit(): void {
    $(document).ready(function () {
      $('#shoppingList').trigger('click');

    })

  }


  dragStart(event: any, product: any) {
    this.draggedProduct = product;
  }

  drop(event: any) {
    if (this.draggedProduct) {
      let draggedProductIndex = this.findIndex(this.draggedProduct);
      this.selectedProducts = [...this.selectedProducts, this.draggedProduct];
      this.availableProducts = this.availableProducts.filter((val: any, i: any) => i != draggedProductIndex);
      this.draggedProduct = null;
    }
  }

  dragEnd(event: any) {
    this.draggedProduct = null;
  }

  findIndex(product: any) {
    let index = -1;
    for (let i = 0; i < this.availableProducts.length; i++) {
      if (product.id === this.availableProducts[i].id) {
        index = i;
        break;
      }
    }
    return index;
  }

  storeLists() {


    this.loadingSpinner = true;  //    page load spinner 
    this.api.storeLists().subscribe(
      (response: any) => {
        if (JSON.stringify(response.data)) {

          this.loadingSpinner = false;  //    page load spinner 
        }
        this.messageNotFound = 'No Record Found';
        this.storeListsArray = response.data;
      }, (err: any) => {
        console.log(err)
      }
    )
  }

  shopList(month: any) {
    this.loadingSpinner = true;  //    page load spinner 
    var closepopup = document.getElementById('Select-options');
    if (closepopup)
      closepopup.click();

    var someDate = new Date();
    var shopListMonth = someDate.setMonth(someDate.getMonth() + +month);
    var shopListDate = someDate.toISOString().slice(0, 10);

    let csomeDate = new Date();
    var cshopListDate = csomeDate.toISOString().slice(0, 10);

    if (month == 0) {
      shopListDate = cshopListDate;
    } else {
      shopListDate = shopListDate;
    }



    this.avaliabletill = cshopListDate

    var data = {};
    data = {
      month: shopListDate
    }
    //  alert(JSON.stringify(data));
    this.api.shopList(data).subscribe(
      (response: any) => {
        this.messageNotFound = 'No Record Found';
        if (JSON.stringify(response.data)) {
          //  this.hideloader();
          this.loadingSpinner = false;  //    page load spinner 
        }

        this.shopListArray = response.data;
        var cshoplistarray = response.data;
        var product_storearray = [];
        for (var $i = 0; $i < cshoplistarray.length; $i++) {
          if (cshoplistarray[$i].products_store) {
            let product_store = cshoplistarray[$i].products_store;

            product_storearray.push(product_store);
          }
        }

        var product_storearray = product_storearray.filter(onlyUnique);

        if (product_storearray)
          this.product_storeArray = product_storearray;
        function onlyUnique(value: any, index: any, self: any) {
          return self.indexOf(value) === index;
        }


      }, (err: any) => {
        console.log(err)
      }
    );

    setInterval(function () {
      $('.cst-cat-title').each(function () {
        if ($(this)) {
          var clisdjkj = $(this).attr('id');
          if (clisdjkj) {
            var hjjj = clisdjkj.replaceAll(/\s/g, '_');
            $(this).attr('id', hjjj.replace(/[^a-zA-Z ]/g, ""));
          }
        }
      });

      $('.cst-cat-title').each(function () {
        if ($(this)) {
          var clisdjkj = $(this).attr('id');
          if (clisdjkj) {
            var hjjj = clisdjkj.replaceAll(/\s/g, '_');

            $(this).closest('.cst-outer-card-body').nextAll('.cst-outer-card-body').find('#' + hjjj).closest('.cst-outer-card-body').insertAfter($('#' + hjjj + ':eq(0)').closest('.cst-outer-card-body'))
            $(this).closest('.cst-outer-card-body').nextAll('.cst-outer-card-body').find('#' + hjjj).hide();
          }
        }
      });
    });



  }
  products_stocks_lasts(products_stocks_lasts: any) {
    throw new Error('Method not implemented.');
  }
  deleteProduct(products_id: any) {

    var data = {
      products_id: products_id
    }

    var x = confirm("Are you sure you want to delete?");
    if (x) {
      this.api.deleteProduct(data).subscribe(
        (response: any) => {
          Swal.fire({
            title: 'Product Deleted Successfully.',
            text: 'Thanks!',
            icon: 'success',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })

          // this.shopList();
        }, (err: any) => {
          Swal.fire({
            title: 'Oops!Something went wrong.',
            text: 'Thanks!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })
          console.log(err)
        }
      )
      return true;
    } else {
      return false;
    }

  }

  categorieslist() {
    this.api.categoriesListing().subscribe(
      (response: any) => {
        this.categoresArray = response.data;
        // console.log(response);
      }, (err: any) => {
        console.log(err)
      }
    )
  }


  selectedcatgeory: any = "";
  selectedstore: any = "";
  selectedmonth: any = "";

  filtershoppinglist(event: any) {


   
    var category = this.selectedcatgeory;
    var store = this.selectedstore;
    var month = parseInt(this.selectedmonth);

    //console.log(category);
    console.log(store);
    console.log(month);

    if (month && Number.isInteger(month)) {
      var someDate = new Date();
      someDate.setMonth(someDate.getMonth() + +month);

      var shopListDate = someDate.toISOString().slice(0, 10);
    } else {

      var shopListDate = '';
    }

    let csomeDate = new Date();
    var cshopListDate = csomeDate.toISOString().slice(0, 10);

    if (month == 0) {
      shopListDate = cshopListDate;
    } else {
      shopListDate = shopListDate;
    }
    this.avaliabletill = cshopListDate
    var data = {};
    data = {
      category: category,
      store: store,
      month: shopListDate,
    }

    this.api.shopFilterList(data).subscribe(
      (response: any) => {
        this.messageNotFound = 'No Record Found';
        if (JSON.stringify(response.data)) {

          this.loadingSpinner = false;  //    page load spinner 
        }

        this.shopListArray = response.data;

        var cshoplistarray = response.data;
        var product_storearray = [];
        for (var $i = 0; $i < cshoplistarray.length; $i++) {
          if (cshoplistarray[$i].products_store) {
            let product_store = cshoplistarray[$i].products_store;

            product_storearray.push(product_store);
          }
        }

        var product_storearray = product_storearray.filter(onlyUnique);

        if (product_storearray)
          this.product_storeArray = product_storearray;
        function onlyUnique(value: any, index: any, self: any) {
          return self.indexOf(value) === index;
        }

      }, (err: any) => {
        console.log(err)
      }
    );

    setInterval(function () {
      $('.cst-cat-title').each(function () {
        if ($(this)) {
          var clisdjkj = $(this).attr('id');
          if (clisdjkj) {
            var hjjj = clisdjkj.replaceAll(/\s/g, '_');
            $(this).attr('id', hjjj.replace(/[^a-zA-Z ]/g, ""));
          }
        }
      });

      $('.cst-cat-title').each(function () {
        if ($(this)) {
          var clisdjkj = $(this).attr('id');
          if (clisdjkj) {
            var hjjj = clisdjkj.replaceAll(/\s/g, '_');
            $(this).closest('.cst-outer-card-body').nextAll('.cst-outer-card-body').find('#' + hjjj).closest('.cst-outer-card-body').insertAfter($('#' + hjjj + ':eq(0)').closest('.cst-outer-card-body'))
            $(this).closest('.cst-outer-card-body').nextAll('.cst-outer-card-body').find('#' + hjjj).hide();

          }
        }
      });
    });

  }


  // remove current row when click on delete.
  deleteSOftProduct($id: any) {
    var x = confirm("Are you sure you want to delete?");
    if (x) {

      if ($('table tr#' + $id).closest('.cst-outer-card-body').find('table tbody tr').length < 2) {
        $('table tr#' + $id).closest('.cst-outer-card-body').remove();
        $('table tr#' + $id).remove();
      }
      else {
        $('table tr#' + $id).remove();
      }

    }
    return false;
  }
  // print a shopping list when click on print button.
  print_shopping_list() {

    var divToPrint = (<HTMLInputElement>document.querySelectorAll('.dashboard-listing.manage-items-listing.desired-shoping')[0]);

    var mywindow = window.open("", "_self")

    const style = `<style>@media print
{

  
  table.table.for-desired-shoping.desired-heading-table {
    width: 95%;
    float: right;
}

    .dashboard-listing table thead tr {
        background: none;
        box-shadow: none;
        height: auto;
        margin-bottom: 0;
    }
    .dashboard-listing table tr {
float: left;
width: 100%;
min-height: 63px;
background: #fbfbfb;
border-radius: 10px;
padding-top: 8px;
padding-left: 20px;
padding-right: 20px;
margin-bottom: 20px;
box-shadow: none;
color: #989898;
font-weight: 300;
border: 0;
vertical-align: middle;
display: inline-table;
}

.Category-div {
float: left;
width: 100%;
color: #fb369a;
font-size: 24px;
margin-bottom: 0;
font-weight: 500;
}

.Category-div.Sub-category {
font-size: 19px;
float: left;
width: auto;
font-weight: 400;
padding-left: 30px;
}

.Category-div.Sub-Sub-category {
float: left;
width: auto;
font-size: 18px;
font-weight: 400;
margin-bottom: 13px;
}

.sub-category-main-div .fas.fa-arrow-right {
color: #fb369a;
font-size: 19px;
float: left;
margin: 4px 15px 0 15px;
}

.sub-category-main-div {
float: left;
width: 100%;
}

.dashboard-listing.manage-items-listing.desired-shoping th:first-child, .dashboard-listing.manage-items-listing.desired-shoping td:first-child {
  width: 18%;
}

.dashboard-listing.manage-items-listing.desired-shoping th:nth-child(2), .dashboard-listing.manage-items-listing.desired-shoping td:nth-child(2) {
  width: 36%;
}

.dashboard-listing.manage-items-listing.desired-shoping th, .dashboard-listing.manage-items-listing.desired-shoping td {
  text-align: center;
  width: 20%;
}

.dashboard-listing.manage-items-listing.desired-shoping th:nth-child(4), .dashboard-listing.manage-items-listing.desired-shoping td:nth-child(4) {
  width: 15%;
}

.dashboard-listing.manage-items-listing.desired-shoping th:nth-child(5), .dashboard-listing.manage-items-listing.desired-shoping td:nth-child(5) {
  width: 10%;
}

.custom-control-label, .custom-control-input{
  opacity: 0;
}

}</style>`;

    (mywindow ? mywindow.document.write('<html><head><title>' + document.title + '</title>') : '');
    (mywindow ? mywindow.document.write(style + '</head><body >') : '');
    (mywindow ? mywindow.document.write('<h1>' + document.title + '</h1><h2>My Shopping List</h2>') : '');
    (mywindow ? mywindow.document.write(divToPrint.outerHTML) : '');
    (mywindow ? mywindow.document.write('</body></html>') : '');

    (mywindow ? mywindow.document.close() : ''); // necessary for IE >= 10
    (mywindow ? mywindow.focus() : ''); // necessary for IE >= 10*/
    (mywindow ? mywindow.print() : '');
    (mywindow ? mywindow.close() : '');

    return true;


  }
  // fucntion to create a quick item.
  add_quick_item(event: any) {
    var quickinput = $(event.target).closest('.add-quick-input').find('.quick-input').val();

    this.quickinput = quickinput;
    var fd;
    if (quickinput !== "") {
      this.loading = true;
      var total_usage_days = 2 * 30;
      var someDate = new Date();
      var numberOfDaysToAdd = total_usage_days;
      someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
      var rightNow = new Date(someDate);
      var res = rightNow.toISOString().slice(0, 10).replace(/-/g, "-");

      let newquickitemarray = {
        'products_name': this.quickinput, 'products_quantity': '1', 'products_price': ($('.quickitem-price').text() ? $('.quickitem-price').text().replace('$', '') : ''), 'products_shelf_life': '30',
        'products_stocks_lasts': res,
        'products_notes': '',
        'products_category_id': '9999',
        'products_sub_cat_id': '9999',
        'default_store': '9999'
      };

      fd = newquickitemarray;

    }

    if (fd) {
      var existquickitemproductdataArray = [];
      if (localStorage.quickitemproductdata)
        var existquickitemproductdata = JSON.parse(localStorage.quickitemproductdata);
      if (existquickitemproductdata)
        existquickitemproductdataArray = existquickitemproductdata;
      existquickitemproductdataArray.push(fd);
      if (existquickitemproductdataArray) {
        localStorage.quickitemproductdata = JSON.stringify(existquickitemproductdataArray);

        this.loading = false;
        Swal.fire({
          title: 'Quick item Added Successfully',
          text: 'You will find product inside quick item group',
          icon: 'success',
          showCancelButton: true,
          confirmButtonText: 'Ok',
          cancelButtonText: 'Cancel'
        })
        this.getQuickitemdata();
      }
    }


  }


  getQuickitemdata() {

    var existquickitemproductdata = JSON.parse(localStorage.quickitemproductdata);
    if (existquickitemproductdata)
      this.quizitemproductsArray = existquickitemproductdata;

  }

}
