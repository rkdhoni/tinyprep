import { Component, OnInit } from '@angular/core';
import { BackendapiService } from '../services/backendapi.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  email: any;
  loading: boolean = false;
  formGroup= new FormGroup({});
  constructor(private api: BackendapiService, private router: Router,private _fb: FormBuilder ) { }

    ngOnInit(): void {
      
      this.formGroup = this._fb.group({
         'email': ['',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]]
        
      })
    }


    forgotPassword(){
      this.loading = true;
      var data = {};
      data = {
                email: this.email  
              }
               
          this.api.forgotPassword(data).subscribe(
          (response: any) => {
            console.log(response);
            this.loading = false;
            var res = response;
            Swal.fire({
              title: 'Email Sent ,Please check your mail.',
              text: 'Thanks!',
              icon: 'success',
              // showCancelButton: true,
              confirmButtonText: 'Ok',
              //cancelButtonText: 'Cancel'
            })   
            }, (err: any) => {
              console.log(err);
              this.loading = false;
              Swal.fire({
                title: 'No user found',
                text: 'Thanks!',
                icon: 'warning',
                // showCancelButton: true,
                confirmButtonText: 'Ok',
                //cancelButtonText: 'Cancel'
              })   
            console.log(err)
          }
        )
  }
}
