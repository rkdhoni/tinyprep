import { Component, OnInit } from '@angular/core';
import { BackendapiService } from '../services/backendapi.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-shopping-lists',
  templateUrl: './shopping-lists.component.html',
  styleUrls: ['./shopping-lists.component.css']
})
export class ShoppingListsComponent implements OnInit {
  shoppingListsArray: any;
  activeCategoryId: string | undefined;
  // products_store: any;
  // products_id: any;
  ShowSubmitLoader: boolean | undefined;
  products_id: any;
  products_name: any;
  products_price: any;
  products_quantity: any;
  products_unit: any;
  products_fav_store: any;
  id: any;
  products_store: any;
  products_total: any=0;
  store_name: any;
  category_name: any;
  categoriesArray: any;
  storeArray: any;
  months:any;
  products_shelf_life: any;
  products_stocks_lasts: any;
  products_notes: any;
  products_expiry_date: any;
  products_image:any;
  formGroup= new FormGroup({});

  constructor(private api: BackendapiService, private router: Router, private route: ActivatedRoute,private _fb: FormBuilder) {
    var user = localStorage.getItem('token');
    var userType = localStorage.getItem('type');  
  this.categories();
  this.storeLists();
    if (user && (userType == '2')) {                               //user is used to restrict page access 
      // this.router.navigate(['/list']);
      this.shoppingLists();
    } else {
      // No user is signed in.
      this.router.navigate(['/']);
    }

  }

  ngOnInit(): void {
   this.formGroup = this._fb.group({
        'products_name': ['',[Validators.required,Validators.minLength(3)]],
        'products_price': ['',[Validators.required]],
        'products_quantity':  ['',[Validators.required]],
        'products_unit':  ['',[Validators.required]],
        'products_store':  ['',[Validators.required]],
        'store_name':  ['',[Validators.required]],
        'category_name':  ['',[Validators.required,Validators.minLength(3)]],
        'products_shelf_life':  ['',[Validators.required]],
        'products_stocks_lasts':  ['',[Validators.required]],
        'products_notes':  ['',[Validators.required]],
        'products_expiry_date':  ['',[Validators.required]],
        'products_image': ['',[Validators.required]]
       
    })
  }
  
  shoppingLists() {

    this.api.shoppingLists().subscribe(
      (response: any) => {
        this.shoppingListsArray = response.data;
                this.shoppingListsArray.forEach((element: { products_price: any; }) => {
                this.products_total= element.products_price+this.products_total;
          });
      }, (err: any) => {
        console.log(err)
      }
    )
  }
  searchApi() {
    // debugger
    var data = {};
      data = {
        products_name: this.products_name
       
      }
    //const fd = new FormData();
    //fd.append('products_name',this.products_name) 
        this.api.searchApi(data).subscribe(
          (response: any) => {
            this.shoppingListsArray = response.data;
            //this.storeLists();
          }, (err: any) => {
            console.log(err)
          }
        )
    }
    storeLists() {
      this.api.storeLists().subscribe(
        (response: any) => {
          this.storeArray = response.data;
        }, (err: any) => {
          console.log(err)
        }
      )
    }
    categories() {
      this.api.categories().subscribe(
        (response: any) => {
          this.categoriesArray = response.data;
        }, (err: any) => {
          console.log(err)
        }
      )
    }

    searchCatStore() {
      var data = {};
        data = {
          store_name: this.store_name,
          category_name: this.category_name,
          months:this.months
        }
    
          this.api.searchCatStore(data).subscribe(
            (response: any) => {
              this.shoppingListsArray = response.data;
              //this.storeLists();
            }, (err: any) => {
              console.log(err)
            }
          )
      }
  deleteProduct(products_id:any){
   
    var data = {
      products_id:products_id
    }
   
      var x = confirm("Are you sure you want to delete?");
      if (x){
        this.api.deleteProduct(data).subscribe(
          (response :any) => {
            this.shoppingLists();
           }, (err:any) => {
           console.log(err)
          }
        )
        return true;
      }else{
        return false;
      }

  }
  addToFavorite() {

    var data = {}
    data = {
      products_id : this.products_id,
    }
   
    if (data != '') {
      Swal.fire({
        title: 'Store Added To Favorite Successfully.',
        text: 'Thanks!',
        icon: 'success',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    } else {
      Swal.fire({
        title: 'Oops! Please try after sometime',
        text: 'Thanks!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    }
    this.api.addToFavorite(data).subscribe(
      (response: any) => {
        this.shoppingLists();
      }, (err: any) => {
        console.log(err)
      }
    )
  }

  editProduct(products_id: any, products_name: any,products_price:any, products_unit: any, products_quantity: any, products_store: any,  products_stocks_lasts: any, products_shelf_life:any,products_expiry_date:any,products_notes:any,products_image:any) {
    // alert('inn');
    // alert(products_id);
    this.products_id = products_id;
    this.products_name = products_name;
    this.products_price = products_price;
    this.products_unit = products_unit;
    this.products_quantity = products_quantity;
    this.products_store = products_store;
    this.products_stocks_lasts = products_stocks_lasts;
    this.products_shelf_life = products_shelf_life;
    this.products_expiry_date = products_expiry_date;
    this.products_notes = products_notes;
    this.products_image = products_image;
  }
  editProducts() {

    var data = {}
    data = {
      products_id: this.products_id,
      products_name: this.products_name,
      products_price: this.products_price,
      products_unit: this.products_quantity ,
      products_quantity: this.products_unit,
      products_store: this.products_store,
      products_stocks_lasts:this.products_stocks_lasts,
      products_shelf_life: this.products_shelf_life,
      products_expiry_date:this.products_expiry_date,
      products_notes:this.products_notes,
      products_image:this.products_image,
      products_category_id: this.id
    }
     if ((this.products_name == '' || this.products_name == undefined || this.products_name.length < '3' ) ||
      (this.products_quantity == '' || this.products_quantity == undefined)||
      (this.products_price == '' || this.products_price == undefined)||
      (this.products_unit == '' || this.products_unit == undefined)||
      (this.products_stocks_lasts == '' || this.products_stocks_lasts == undefined)||
      (this.products_shelf_life == '' || this.products_shelf_life == undefined)||
      (this.products_notes == '' || this.products_notes == undefined)||
      (this.products_expiry_date == '' || this.products_expiry_date == undefined)||
      (this.products_image == '' || this.products_image == undefined)){
     
        Swal.fire({
          title: 'Oops! Please Enter Valid details',
          text: 'Thanks!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Ok',
          cancelButtonText: 'Cancel'
        })
      }else{
         Swal.fire({
          title: 'Product Updated Successfully',
          text: 'Thanks!',
          icon: 'success',
          showCancelButton: true,
          confirmButtonText: 'Ok',
          cancelButtonText: 'Cancel'
        })
         this.api.editProducts(data).subscribe(
      (response: any) => {
        this.shoppingLists();
      }, (err: any) => {
        console.log(err)
      }
    )
    }
    // if (data != '') {
    //   Swal.fire({
    //     title: 'Product Updated Successfully',
    //     text: 'Thanks!',
    //     icon: 'success',
    //     showCancelButton: true,
    //     confirmButtonText: 'Ok',
    //     cancelButtonText: 'Cancel'
    //   })
    // } else {
    //   Swal.fire({
    //     title: 'Oops! Please try after sometime',
    //     text: 'Thanks!',
    //     icon: 'warning',
    //     showCancelButton: true,
    //     confirmButtonText: 'Ok',
    //     cancelButtonText: 'Cancel'
    //   })
    // }
   
  }
  
}
