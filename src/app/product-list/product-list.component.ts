import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import Swal from "sweetalert2";
import { BackendapiService } from "../services/backendapi.service";
import { FormGroup, Validators, FormBuilder, FormControl } from "@angular/forms";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.css"],
})
export class ProductListComponent implements OnInit {
  [x: string]: any;
  id: any;
  name: string | undefined;
  productsArray: any;
  products_id: any;
  products_name: any;
  products_price: any;
  products_quantity: any;
  products_unit: any;
  products_store: any;
  products_stocks_lasts: any;
  products_shelf_life: any;
  products_expiry_date: any;
  products_notes: any;
  products_image: any;
  stores_name: any;
  store_name: any = 0;
  storeArray: any;
  categoriesArray: any;
  category_name: any = 0;
  imageDirectoryPath: any = "https://enacteservices.net/tinyprepbackend/";
  selctedProduct: any;
  formGroup = new FormGroup({});

  productsView: any;
  productsName: any;
  subCategories_id: any;
  loading: boolean = false;
  subCategories_name: any;
  messageError: any;
  storeListsArray: any;
  default_store: any;
  subcategoriesArray: any;
  selectElementText: any;
  messageNotFound: any;
  imageSrc: string | ArrayBuffer | any = "";

  if_records: boolean = true;
  fileChanges: boolean = false;
  filesImage: any = [];
  activeI: any;

  classexit: boolean = false;
  submitdisable: any = false;
  booleanUpdate: any = false

  products_shelf_life_error:any="";

  
  defaultimage: any = "http://i.pravatar.cc/500?img=7";


  constructor(
    private api: BackendapiService,
    private router: Router,
    private route: ActivatedRoute,
    private _fb: FormBuilder
  ) {

    this.categories();
    this.removeUpload();

  }

  ngOnInit(): void {

    this.allProductsLists();

    this.formGroup = this._fb.group({
      products_name: ["", [Validators.required, Validators.minLength(3)]],
      products_price: ["", [Validators.required]],
      products_quantity: ["", [Validators.required]],
      products_unit: ["", [Validators.required]],
      products_store: ["", [Validators.required]],
      store_name: ["", [Validators.required]],
      category_name: ["", [Validators.required, Validators.minLength(3)]],
      products_shelf_life: ["", [Validators.required]],
      products_stocks_lasts: ["", [Validators.required]],
      products_notes: ["", [Validators.required]],
      products_expiry_date: ["", [Validators.required]],
      products_image: ["", [Validators.required]],
    });
    this.messageNotFound = "No Record Found";


    this.myGroup = this._fb.group({
      'category_name': ['', [Validators.required]],
      'store_name': ['', [Validators.required]],
    });

  }

  // validateAllFields(myGroup: FormGroup) {
  //   Object.keys(myGroup.controls).forEach(field => {
  //     const control = myGroup.get(field);
  //     if (control instanceof FormControl) {
  //       control.markAsTouched({ onlySelf: true });
  //     } else if (control instanceof FormGroup) {
  //       this.validateAllFields(control);
  //     }
  //   });
  // }





  allProductsLists() {
    this.api.allProductsLists().subscribe(
      (response: any) => {
        this.productsArray = response.data;
        console.log("All products::", this.productsArray);
      }, (err: any) => {
        console.log(err)
        console.log("All products::", this.productsArray);
      }
    );
  }

  subcategories() {
    var data = {};
    data = {
      subCategories_cat_id: this.selectElementText,
    };
    this.api.subcategoryLists(data).subscribe(
      (response: any) => {
        this.subcategoriesArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }




  viewProduct(products_id: any) {

    var data = {
      products_id: products_id
    }

    this.api.viewProduct(data).subscribe(
      (response: any) => {
        this.productsView = response.data;
        console.log("product view : ", this.productsView);
      }, (err: any) => {
        console.log(err)
      }
    )
    return true;
  }


  deleteProduct(products_id: any) {
    var data = {
      products_id: products_id
    }

    var x = confirm("Are you sure you want to delete?");
    if (x) {
      this.api.deleteProduct(data).subscribe(
        (response: any) => {
          Swal.fire({
            title: 'Product Deleted Successfully.',
            text: 'Thanks!',
            icon: 'success',
            confirmButtonText: 'Ok',
          })
          this.allProductsLists();
        }, (err: any) => {
          Swal.fire({
            title: 'Oops!Something went wrong.',
            text: 'Thanks!',
            icon: 'warning',
            confirmButtonText: 'Ok',
          })
          console.log(err)
        }
      )
      return true;
    } else {
      return false;
    }

    var x = confirm("Are you sure you want to delete?");
    if (x) {
      this.api.deleteProduct(data).subscribe(
        (response: any) => {
          Swal.fire({
            title: "Product Deleted Successfully.",
            text: "Thanks!",
            icon: "success",
            confirmButtonText: "Ok",
          });
          this.allProductsLists();
        },
        (err: any) => {
          Swal.fire({
            title: "Oops!Something went wrong.",
            text: "Thanks!",
            icon: "warning",
            confirmButtonText: "Ok",
          });
          console.log(err);
        }
      );
      return true;
    } else {
      return false;
    }
  }

  removeUpload() {

    /*$(document).on("click", "button.remove-image", function () {
      $(this)
        .closest(".file-upload")
        .find(".file-upload-image")
        .removeAttr("src");
      $(this).closest(".file-upload").find(".default-upload-image").show();
      $(this).closest(".image-title-wrap").hide();
      this.products_image="";
    });*/
    //console.log(this.products_image)
    this.products_image="";
  }


  searchApi() {

    if ((this.productsName == '') || (this.productsName == undefined)) {

      Swal.fire({
        title: 'Oops! Please Enter Product Name.',
        text: 'Thanks!',
        icon: 'warning',
        confirmButtonText: 'Ok',
      })
    } else {
      this.loading = true;
      var data = {};
      data = {
        products_name: this.productsName,
      };

      this.api.searchApi(data).subscribe(

        (response: any) => {
          this.loading = false;

          if (response.status == 1) {
            this.if_records = true;
            this.productsArray = response.data;
          } else {
            this.if_records = false;
          }
        },
        (err: any) => {
          this.loading = false;
          console.log(err);
        }
      );
    }
  }

  storeListsSearch() {

    this.category_name = 0;
    this.store_name = 0;

    this.loading = true;
    this.api.storeLists().subscribe(
      (response: any) => {
        this.loading = false;
        this.storeArray = response.data;
      },
      (err: any) => {
        this.loading = false;
        console.log(err);
      }
    );
  }




  categories() {
    this.api.categories().subscribe(
      (response: any) => {
        this.categoriesArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  @ViewChild("myModalClose_BTn") myModalClose_BTn!: ElementRef;

  searchProducts(e: any) {

    this.loading = true;
    var data = {};


    data = {
      store_id: this.store_name,
      category_id: this.category_name,
    };


    this.api.searchProducts_filter(data).subscribe(
      (response: any) => {

        this.loading = false;

        if (response.data == null || response.data === 0 || response.data.length == 0) {
          Swal.fire({
            title: "Oops! No Product Found !!",
            text: "Thanks!",
            icon: "warning",
            confirmButtonText: "Ok",
          });
        }

        this.productsArray = response.data;
        this.myModalClose_BTn.nativeElement.click();
        //this.myGroup.reset();

      },
      (err: any) => {
        this.loading = false;
        this.myModalClose_BTn.nativeElement.click();
        this.myGroup.reset();
      }
    );
  }


  addProducts() {
    this.loading = true;

    const fd = new FormData();
    fd.append("products_image", this.selectedFile, this.selectedFile.name);
    fd.append("products_name", this.products_name);
    fd.append("products_quantity", this.products_quantity);
    //  fd.append('products_unit',this.products_unit)
    fd.append("products_price", this.products_price);
    //  fd.append('products_stocks_lasts',this.products_stocks_lasts)
    fd.append("products_shelf_life", this.products_shelf_life);
    //  fd.append('products_expiry_date',this.products_expiry_date)
    fd.append("products_notes", this.products_notes);
    fd.append("products_category_id", this.id);
    fd.append("products_sub_cat_id", this.selectElementText);
    //  if(this.default_store.length >0){
    //   // fd.append('products_store',this.productsArray[0].products_store)
    //    fd.append('products_store',this.default_store)
    //  }
    //  if(this.default_store == 0){
    //    fd.append('products_store',this.products_store)
    //  }

    // alert(JSON.stringify(fd));
    // var data = {};
    // if(this.default_store == 0){
    //   data = {

    //     products_category_id: this.id,
    //     products_name: this.products_name,
    //     products_quantity: this.products_quantity,
    //     products_unit: this.products_unit,
    //     products_price: this.products_price,
    //     products_store: this.productsArray[0].products_store,
    //     products_stocks_lasts: this.products_stocks_lasts,
    //     products_shelf_life: this.products_shelf_life,
    //     products_expiry_date: this.products_expiry_date,
    //     products_notes: this.products_notes,
    //     products_image: this.products_image
    //   }
    // }else{
    //   data = {
    //     products_category_id: this.id,
    //     products_name: this.products_name,
    //     products_quantity: this.products_quantity,
    //     products_unit: this.products_unit,
    //     products_price: this.products_price,
    //     products_store: this.default_store,
    //     products_stocks_lasts: this.products_stocks_lasts,
    //     products_shelf_life: this.products_shelf_life,
    //     products_expiry_date: this.products_expiry_date,
    //     products_notes: this.products_notes,
    //     products_image: this.products_image
    //    }
    // }
    // if ((this.products_name == '' || this.products_name == undefined || this.products_name.length < '3' ) ||
    //  (this.products_quantity == '' || this.products_quantity == undefined)||
    //  (this.products_price == '' || this.products_price == undefined)||
    //  (this.products_notes == '' || this.products_notes == undefined)||
    //  (this.products_sub_cat_id == '0')){

    //    Swal.fire({
    //      title: 'Oops! Please Enter Valid details',
    //      text: 'Thanks!',
    //      icon: 'warning',
    //      showCancelButton: true,
    //      confirmButtonText: 'Ok',
    //      cancelButtonText: 'Cancel'
    //    })
    //  }else
    //  {

    this.api.addProducts(fd).subscribe(
      (response: any) => {
        var res = response;
        if (res.error == 1) {
          this.loading = false;
          Swal.fire({
            title: "Oops! Please Enter Valid details",
            text: "Thanks!",
            icon: "warning",
            confirmButtonText: "Ok",
          });
          this.messageError = res.message;
        } else {
          this.loading = false;
          this.messageError = "";
          Swal.fire({
            title: "Product Added Successfully",
            text: "You will find product inside category",
            icon: "success",
            confirmButtonText: "Ok",
          });
          this.productsArray = response.data;

        }
      },
      (err: any) => {
        this.loading = false;
        console.log(err);
        Swal.fire({
          title: "Oops! Please Enter Valid details",
          text: "Thanks!",
          icon: "warning",
          confirmButtonText: "Ok",
        });

      }
    );

  }

  getSelectedOptionText(event: any) {
    let selectedOptions = event.target["options"];
    let selectedIndex = selectedOptions.selectedIndex;
    this.selectElementText = selectedOptions[selectedIndex].value;
    this.subcategories();
  }



  selectedFile: any = null;
  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }


  // editProduct(
  //   products_id: any,
  //   products_name: any,
  //   products_price: any,
  //   products_unit: any,
  //   products_quantity: any,
  //   products_store: any,
  //   products_stocks_lasts: any,
  //   products_shelf_life: any,
  //   products_expiry_date: any,
  //   products_notes: any,
  //   products_image: any
  // ) {
  //   //  debugger;
  //   // alert('inn');
  //   alert(products_id);
  //   this.products_id = products_id;
  //   this.products_name = products_name;
  //   this.products_price = products_price;
  //   this.products_unit = products_unit;
  //   this.products_quantity = products_quantity;
  //   this.products_store = products_store;
  //   this.products_stocks_lasts = products_stocks_lasts;
  //   this.products_shelf_life = products_shelf_life;
  //   this.products_expiry_date = products_expiry_date;
  //   this.products_notes = products_notes;
  //   this.products_image = products_image;
  //   //this.selctedProduct =
  //   // this.productForm.get('products_name')?.setValue(product.products_name)
  // }

  editProduct(
    products_id: any,
    products_name: any,
    products_quantity: any,
    products_shelf_life: any,
    products_notes: any,
    products_image: any,
    i: any
  ) {

    this.products_id = products_id;
    // alert(this.products_id);
    this.products_name = products_name;
    //alert(this.products_name); 
    this.products_quantity = products_quantity;
    // alert(this.products_quantity); 
    this.products_shelf_life = products_shelf_life;
    //alert(this.products_shelf_life);
    this.products_notes = products_notes;
    //alert(this.products_notes);
    this.products_image = products_image;
    // alert(this.products_image);

    // console.log(this.imageSource);
    // console.log(i);
    // console.log(this.imageSource[i]);

    // this.imageSource[i] = this.imageDirectoryPath + "" + this.products_image;
    //alert(this. this.imageSource[i]);

   

   // this.hidetd[i] = true;
    this.activeI = i;

    // for (var $i = 0; $i < this.hidetd.length; $i++) {
    //   if ($i !== i) this.hidetd[$i] = false;
    // }
    var cid = $(".open-modal-popup-item").attr("data-target");
    if (cid) {
      $(cid)
        .find(".file-upload .file-upload-image")
        .attr("src", this.imageDirectoryPath + products_image);
    }
  }


  editProducts() {
    this.loading = true;
    const fd = new FormData();
    fd.append("products_id", this.products_id);
    fd.append("products_name", this.products_name);
    fd.append("products_quantity", this.products_quantity);
    fd.append("products_price", this.products_price);
    fd.append("products_unit", this.products_unit);
    fd.append("products_store", this.products_store);
    fd.append("products_stocks_lasts", this.products_stocks_lasts);
    fd.append("products_shelf_life", this.products_shelf_life);
    fd.append("products_expiry_date", this.products_expiry_date);
    fd.append("products_notes", this.products_notes);
    fd.append("products_image", this.selectedFile, this.selectedFile.name);
    fd.append("products_category_id", this.id);

    if (
      this.products_name == "" ||
      this.products_name == undefined ||
      this.products_name.length < "3" ||
      this.products_quantity == "" ||
      this.products_quantity == undefined ||
      this.products_price == "" ||
      this.products_price == undefined ||
      this.products_unit == "" ||
      this.products_unit == undefined ||
      this.products_stocks_lasts == "" ||
      this.products_stocks_lasts == undefined ||
      this.products_shelf_life == "" ||
      this.products_shelf_life == undefined ||
      this.products_notes == "" ||
      this.products_notes == undefined ||
      this.products_expiry_date == "" ||
      this.products_expiry_date == undefined 
      // this.products_image == "" ||
      //this.products_image == undefined
    ) {
      this.loading = false;
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        text: "Thanks!",
        icon: "warning",
        //showCancelButton: true,
        confirmButtonText: "Ok",
        //cancelButtonText: "Cancel",
      });
    } else {
      this.api.editProducts(fd).subscribe(
        (response: any) => {
          console.log(response);
          var res = response;
          if (res.error == 1) {
            this.loading = false;
            Swal.fire({
              title: res.message,
              text: "Thanks!",
              icon: "warning",
              //showCancelButton: true,
              confirmButtonText: "Ok",
              //cancelButtonText: "Cancel",
            });
            this.messageError = res.message;
          } else {
            this.messageError = "";
            this.loading = false;
            Swal.fire({
              title: "Product Updated Successfully",
              text: "Thanks!",
              icon: "success",
              // showCancelButton: true,
              confirmButtonText: "Ok",
              // cancelButtonText: "Cancel",
            });
            this.allProductsLists();
          }
        },
        (err: any) => {
          console.log(err);
          this.loading = false;
          Swal.fire({
            title: "Oops! Please Enter Valid details",
            text: "Thanks!",
            icon: "warning",
            // showCancelButton: true,
            confirmButtonText: "Ok",
            // cancelButtonText: "Cancel",
          });
          console.log(err);
        }
      );
    }
  }
  openNav() {
    // (document.getElementById("openModel") as HTMLElement).click();
    (document.getElementById("mySidebar") as HTMLElement).style.width = "500px";
    (document.getElementById("main") as HTMLElement).style.marginLeft = "500px";
  }

  closeNav() {
    // (document.getElementById("openModel") as HTMLElement).click();
    (document.getElementById("mySidebar") as HTMLElement).style.width = "0";
    (document.getElementById("main") as HTMLElement).style.marginLeft = "0";
  }


  onDragOver(event: any) {
    event.preventDefault();
  }


  onDropSuccess(event: any) {
    event.preventDefault();
    this.onFileChange(event.dataTransfer.files);
  }

  onChange(event: any) {
    event.preventDefault();
    this.onFileChange(event.target.files);
  }


  private onFileChange(files: File[]) {
    const file = files;
    const reader = new FileReader();
    reader.onload = (e) => (this.imageSrc = reader.result);
    this.fileChanges = true;
    this.selectedFile = file[0];
    this.filesImage = file[0];
    reader.readAsDataURL(files[0]);
    $(".default-upload-image").hide();
    $("button.remove-image").closest(".image-title-wrap").show();
  }


  closeProductpopup(
    products_id: any,
    products_name: any,
    products_quantity: any,
    products_shelf_life: any,
    products_notes: any,
    products_image: any,
    i: any
  ) {
    var $this = this;
    setTimeout(function () {
      //  $this.editProduct(products_id, products_name, products_quantity, products_shelf_life, products_notes, products_image, i);

      $(".default-upload-image").hide();
      $("button.remove-image").closest(".image-title-wrap").show();
    }, 1000);
  }


  editItems() {

    this.loading = true;

    var total_usage_days = this.products_quantity * this.products_shelf_life;
    var someDate = new Date();
    var numberOfDaysToAdd = total_usage_days;
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
    var rightNow = new Date(someDate);

    var res = rightNow.toISOString().slice(0, 10).replace(/-/g, "-");

    // if(this.products_shelf_life > 1000){
    //   this.products_shelf_life_error ="Products self life is not geater than 1000";
      
    // }else{
    //   this.products_shelf_life =this.products_shelf_life;
    // }

    const fd = new FormData();
    fd.append("products_id", this.products_id);
    fd.append("products_name", this.products_name);
    fd.append("products_quantity", this.products_quantity);
    fd.append("products_stocks_lasts", res);
    fd.append("products_shelf_life", this.products_shelf_life);
    fd.append("products_notes", this.products_notes);

    console.log(this.products_image);
    // return false; 
    if (this.filesImage.name) {
      fd.append("products_image", this.selectedFile, this.selectedFile.name);
    } else {
      fd.append("products_image", this.products_image);
    }


    if (
      this.products_name == "" ||
      this.products_name == undefined ||
      this.products_name.length < "3" ||
      this.products_quantity == "" ||
      this.products_quantity == undefined 
     
    ) {
      this.loading = false;
      if ($("#myModals").hasClass("show")) {
        $(".closepopup").trigger("click");
      }
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        text: "Thanks!",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    } else {
      this.api.editItems(fd).subscribe(
        (response: any) => {
          Swal.fire({
            title: "Product Updated Successfully",
            text: "Thanks!",
            icon: "success",
            confirmButtonText: "Ok",
          });

          this.loading = false;
          if ($("#myModals").hasClass("show")) {
            $(".closepopup").trigger("click");
          }

          this.allProductsLists();

        },
        (err: any) => {
          Swal.fire({
            title: "Oops! Something went wrong.",
            text: "Thanks!",
            icon: "warning",
            confirmButtonText: "Ok",
          });
          this.loading = false;
          if ($("#myModals").hasClass("show")) {
            $(".closepopup").trigger("click");
          }
          console.log(err);
        }
      );
    }
  }


  filteredProduct() {
    this.loading = true; //    page load spinner
    var data = {
      subCategories_id: this.subCategories_id,
      id: this.id,
    };

    this.api.filteredProduct(data).subscribe(
      (response: any) => {
        if (JSON.stringify(response.data)) {

          this.loading = false; //    page load spinner
        }
        this.messageNotFound = "No Record Found";
        this.productsArray = response.data;
      },
      (err: any) => { }
    );
  }
}
