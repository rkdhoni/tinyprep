import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BackendapiService } from '../services/backendapi.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categoresArray: any = [];
  category: any = "";
  name: any;
  ShowSubmitLoader: boolean = false;
  activeCategoryId: any = "";
  categories_notes: any="";
  imageDirectoryPath: any = 'https://enacteservices.net/tinyprepbackend/';
  categories_image: any;
  stores_name: any;
  stores_link: any;
  categories_name: any;
  category_search:any;
  formGroup = new FormGroup({});
  fd = new FormData();
  id:any;
  // loading = false;
   loading: boolean = false;
  messageError: any;
  categoriesnotes: any;
  constructor(private api: BackendapiService, private router: Router, private route: ActivatedRoute, private http: HttpClient,
    private _fb: FormBuilder) {


      var user = localStorage.getItem('token');
      // var userType = localStorage.getItem('type');  

      if (!user) {                            //user is used to restrict page access 
        this.router.navigate(['/']);
      } 

  
  }

  @ViewChild('myModals_close_btnEdit') myModals_close_btnEdit!:ElementRef;
  ngOnInit(): void {
    this.categoriesListing();
    this.formGroup = this._fb.group({
      //  'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      'categories_name': ['', [Validators.required, Validators.minLength(3)]],
      'categoriesnotes': ['', [Validators.required, Validators.minLength(4)]],
      'categories_image': ['', [Validators.required]],
      'categories_notes': ['', [Validators.required, Validators.minLength(4)]],
      'category': ['', [Validators.required, Validators.minLength(3)]]

    })
  }


  categoriesListing() {
    this.api.categoriesListing().subscribe(
      (response: any) => {
        this.categoresArray = response.data;
        console.log(this.categoresArray);
      }, (err: any) => {
        console.log(err)
      }
    )
  }


  searchApi() {
    this.loading = true;
    // alert('helelo');
  
    var data = {};
    data = {
      categories_name: this.category_search

    }
    //const fd = new FormData();
    //fd.append('products_name',this.products_name) 
    this.api.searchApi(data).subscribe(
      (response: any) => {
        this.loading = false;
        this.categoresArray = response.data;
        console.log(this.categoresArray);
        //this.storeLists();
      }, (err: any) => {
        this.loading = false;
        console.log(err)
      }
    )
  }

  selectedFile: any = null;
  onFileSelected(event: any) {

    this.selectedFile = event.target.files[0];

    

  }


  addCategory() {
    this.loading = true;
    
    this.fd.append('categories_image', this.selectedFile , this.selectedFile.name)
    this.fd.append('categories_name', this.categories_name)
    this.fd.append('categories_notes', this.categoriesnotes)
    
    if ((this.categories_name == '' || this.categories_name == undefined || this.categories_name.length < '3') || (this.categoriesnotes == '' || this.categoriesnotes == undefined || this.categoriesnotes.length < '4') ) {


      Swal.fire({
        title: 'Oops! Please Enter Valid details',
        text: 'Thanks!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    } 
    else {
     
      this.api.addCategory(this.fd).subscribe(
        (response: any) => {
          var res = response;
          if(res.error == 1){
            Swal.fire({
              title: 'Oops! Please Enter Valid details',
              text: 'Thanks!',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Ok',
              cancelButtonText: 'Cancel'
            }) 
            this.messageError = res.message;
          }else{
            
          this.messageError = "";
          Swal.fire({
            title: 'Category Added Successfully',
            text: 'Thanks!',
            icon: 'success',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })
          this.loading = false;
          this.categoriesListing();
        }
      }, (err: any) => {
          this.loading = false;
          Swal.fire({
            title: 'Oops! Something went wrong,Please try again',
            text: 'Thanks!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })
          console.log(err)
        }
      )
    }

  }
 


  editCategory(id: any, cat_name: any,categories_notes:any,categories_image:any) {
   
    this.id = id
    this.category = cat_name;
    this.categories_image = categories_image;
    this.categories_notes = categories_notes;
    
  }


  editCategories() {

        this.loading = true;
        const fd = new FormData();
        this.fd.append('categories_image', this.selectedFile , this.selectedFile.name)
        this.fd.append('name', this.category)
        this.fd.append('categories_notes', this.categories_notes)
        this.fd.append('id', this.id)

        console.log(this.fd);

        this.api.editCategories(this.fd).subscribe(
          (response: any) => {
            var res = response;
            if(res.error == 1){
              this.loading = false;
              this.messageError = res.message;
              this.myModals_close_btnEdit.nativeElement.click();
              Swal.fire({
                title: 'Oops! Please Enter Valid details',
                text: 'Thanks!',
                icon: 'warning',
                //showCancelButton: true,
                confirmButtonText: 'Ok',
                //cancelButtonText: 'Cancel'
              }) 
              
            }else{
              this.messageError = "";
              this.categoriesListing();
              this.loading = false;
              this.myModals_close_btnEdit.nativeElement.click();

              Swal.fire({
                title: 'Category edited successfully',
                text: 'Thanks!',
                icon: 'success',
              // showCancelButton: true,
                confirmButtonText: 'Ok',
              // cancelButtonText: 'Cancel'
              })
            }
        }, (err: any) => {
            this.loading = false;
            this.myModals_close_btnEdit.nativeElement.click();

            Swal.fire({
              title: 'Oops! Something went wrong',
              text: 'Thanks!',
              icon: 'warning',
              //showCancelButton: true,
              confirmButtonText: 'Ok',
              //cancelButtonText: 'Cancel'
            })
            console.log(err)
          }
        )
  
  }

  deleteCategory(id: any) {
    var data = {
      id: id
    }
    var x = confirm("Are you sure you want to delete?");
    if (x) {
      this.api.deleteCategory(data).subscribe(
        (response: any) => {
          this.categoriesListing();
          Swal.fire({
            title: 'Category deleted Successfully',
            text: 'Thanks!',
            icon: 'success',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })
          this.ShowSubmitLoader = false;
          this.activeCategoryId = ""
        }, (err: any) => {
          this.activeCategoryId = ""
          this.ShowSubmitLoader = false;
          Swal.fire({
            title: 'Oops! Something went wrong',
            text: 'Thanks!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })
          console.log(err)
        }
      )
      return true;
    } else {
      return false;
    }
  }

  Logout() {
    localStorage.removeItem("token")
    localStorage.removeItem("type")
    this.router.navigate(['/'])
    // this.router.navigate[('login')]
  }

}
