import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BackendapiService } from "src/app/services/backendapi.service";
import Swal from "sweetalert2";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";
import {
  SocialAuthService,
  GoogleLoginProvider,
  SocialUser,
  FacebookLoginProvider,
} from "angularx-social-login";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"],
})
export class ProfileComponent implements OnInit {
  messageError: any;
  show: boolean = false;
  socialUser: any = SocialUser;
  new_password: any;
  confirm_password: any;
  old_password: any;
  resArray: any;
  userDataArray: any;
  formGroup = new FormGroup({});
  loading: boolean = false;
  password_regex: any =
    "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
  email: any;
  name: any;
  image: any;
  localImageUrl: any;
  defaultimage: any = "http://i.pravatar.cc/500?img=7";
  existingimage: any;
  imageDirectoryPath: any = "https://enacteservices.net/tinyprepbackend/";
  constructor(
    private api: BackendapiService,
    private router: Router,
    private socialAuthService: SocialAuthService,
    private route: ActivatedRoute,
    private http: HttpClient,
    private _fb: FormBuilder
  ) {

      var user = localStorage.getItem('token');
      if (!user) {                            //user is used to restrict page access 
        this.router.navigate(['/']);
      } 

   
  }

  ngOnInit(): void {

   
    // call function to fetch current user data
    this.currentUserData();

    this.formGroup = this._fb.group({
      name: ["", [Validators.required]],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
        ],
      ],
      old_password: ["", [Validators.required]],
      new_password: [
        "",
        [
          Validators.required,
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ],
      ],
      image: ["", [Validators.required]],
      //  'confirm_password': ['',[Validators.required,Validators.minLength(6)]]
    });
  }

  // function to fetch current user data.
  currentUserData() {

    this.api.userData().subscribe(
      (response: any) => {
        this.userDataArray = response.data;
        let userdat = response.data[0];
        this.name = userdat.name;
        this.email = userdat.email;
        this.image = userdat.image;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  selectedFile: any = null;
  onFileSelected(event: any) {

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localImageUrl = event.target.result;

        console.log("Image url :",this.localImageUrl);
      };

      reader.readAsDataURL(event.target.files[0]);
      this.selectedFile = event.target.files[0];

      console.log("Selected file:",this.selectedFile.name);
      let file = event.target.files[0];

      console.log("File :",file);
      (<HTMLInputElement>document.getElementById("imageInput")).value = file
        ? file.name
        : ""; 
    }
  }

  editProfile(){

    //this.loading = true;
    const fd = new FormData();

    if(this.selectedFile) {

      fd.append("image", this.selectedFile, this.selectedFile.name);
    }else {
      fd.append("image_url", this.image);

      console.log("Imageeeee",this.image);
    }

    fd.append("name", this.name);
    fd.append("email", this.email);


    // if(!this.selectedFile || this.image == "null"){

    //   this.loading = false;
    //   Swal.fire({
    //     title: "Oops! Please Enter Valid details",
    //     text: "Please Choose Image First....!!",
    //     icon: "warning",
    //     confirmButtonText: "Ok",
    //   });
    //   return;
    // }
    

    if(
      this.name == "" ||
      this.name == undefined ||  
      this.email == "" ||
      this.email == undefined 
    ){
      this.loading = false;
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }else{
      this.api.editProfile(fd).subscribe(
        (response: any) => {
          console.log(response);
          var res = response;
          this.loading = false;
          if (res.error == 1) {
            Swal.fire({
              title: "Oops! Please Enter Valid details",
              text: "Thanks!",
              icon: "warning",
              confirmButtonText: "Ok",
            });
          } else {
            this.loading = false;
            this.resArray = response.data;
            Swal.fire({
              title: "Profile edited successfully",
              text: "Thanks!",
              icon: "success",
              confirmButtonText: "Ok",
            });


            window.location.reload();

          
 
          }
        },
        (err: any) => {
          this.loading = false;
          console.log(err);
        }
      );
    }
  }


  changePassword() {
    this.loading = true;

    var data = {};
    data = {
      old_password: this.old_password,
      new_password: this.new_password,
    };

    this.api.changePassword(data).subscribe(
      (response: any) => {
        console.log(response);
        var res = response;
        this.loading = false;
        if (res.error == 1) {
          Swal.fire({
            title: "Oops! Please Enter Valid details",
            text: response.message,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
          });

          this.old_password='';
          this.new_password='';
        } else {
          this.loading = false;
          this.resArray = response.data;
          Swal.fire({
            title: "Password Changed successfully",
            text: "Thanks!",
            icon: "success",
            showCancelButton: true,
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
          });
          this.old_password='';
          this.new_password='';
        }
      },
      (err: any) => {
        console.log(err);
        this.loading = false;
        console.log(err);
      }
    );
  }

  loginWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      // alert(JSON.stringify(this.socialUser));
      let data = {
        name: this.socialUser["name"],
        social_id: this.socialUser["id"],
        email: this.socialUser["email"],
        provider: this.socialUser["provider"],
        image: this.socialUser["photoUrl"],
      };
      this.api.socialLogin(data).subscribe(
        (response: any) => {
          var res = response;
          // this.router.navigate(['dashboard'])
          if (res.error == 1) {
            this.messageError = res.message;
          } else {
            this.messageError = "";

            localStorage.setItem("token", res.data["social_id"]);
            localStorage.setItem("type", "2");
            this.router.navigate(["/tinyprep"]);
            this.router.navigate(["dashboard"]);
          }
        },
        (err: any) => {
          console.log(err);
        }
      );
    });
  }

  loginWithFacebook(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      let data = {
        name: this.socialUser["name"],
        social_id: this.socialUser["id"],
        email: this.socialUser["email"],
        provider: this.socialUser["provider"],
        image: this.socialUser["photoUrl"],
      };
      this.api.socialLogin(data).subscribe(
        (response: any) => {
          var res = response;
          // this.router.navigate(['dashboard'])
          if (res.error == 1) {
            this.messageError = res.message;
          } else {
            this.messageError = "";

            localStorage.setItem("token", res.data["social_id"]);
            localStorage.setItem("type", "2");
            this.router.navigate(["/tinyprep"]);
            this.router.navigate(["dashboard"]);
          }
        },
        (err: any) => {
          console.log(err);
        }
      );
    });
  }

  openAppleAuthWindow() {
    const CLIENT_ID = "com.myapp.bundle.backend";
    const REDIRECT_API_URL = "http://localhost:4200/api/auth-apple-signin";
    window.open(
      "https://appleid.apple.com/auth/authorize?" +
        `client_id=${CLIENT_ID}&` +
        `redirect_uri=${encodeURIComponent(REDIRECT_API_URL)}&` +
        "response_type=code id_token&" +
        "scope=name email&" +
        "response_mode=form_post",
      "_blank"
    );
  }

  applelogin() {
    $(".underdevelopment-go-action").trigger("click");
  }


  userData() {
    $('.header-name-right').hide();
    this.api.userData().subscribe(
      (response: any) => {
        var $this = this;
        
         this.userDataArray = response.data[0];
         if(this.userDataArray)
         $('.header-name-right').show();
      }, (err: any) => {
        console.log(err)
      }
    )
  }

}
