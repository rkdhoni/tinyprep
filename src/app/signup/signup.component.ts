import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BackendapiService } from '../services/backendapi.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { SocialAuthService, GoogleLoginProvider, SocialUser, FacebookLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  name: any;
  email: any;
  password: any;
  usersArray: any;
  messageError: any;
  formGroup = new FormGroup({});
  show: boolean = false;
  result: boolean = false;
  loading: boolean = false;
  socialUser: any = SocialUser;
  isLoggedin: boolean | undefined;

  defaultimage: any = "http://i.pravatar.cc/500?img=7";


  regex: any = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  password_regex: any = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
  constructor(private api: BackendapiService, private router: Router, private route: ActivatedRoute, private _fb: FormBuilder, private socialAuthService: SocialAuthService) {

    var user = localStorage.getItem('token');
    var userType = localStorage.getItem('type');

    if (user) {
      if (userType == "1") {
        this.router.navigate(['/categories']);
      } else {

        this.router.navigate(['/dashboard'])

      }

    }

  }

  ngOnInit(): void {
    this.formGroup = this._fb.group({
      //  'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      'name': ['', [Validators.required, Validators.minLength(3)]],
      'email': ['', [Validators.required, Validators.email, Validators.pattern(/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],
      'password': ['', [Validators.required, Validators.minLength(6), Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")]]
    })
  }
  password_show() {
    this.show = !this.show;
  }

  allowAlphabetic(e: any) {
    var regex = new RegExp(/^[a-zA-Z]+$/);
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      e.preventDefault();
      return false;
    }
  }




  addUsers() {

    this.loading = true;
    let EMAIL_REGEXP = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    var data = {};
    data = {
      name: this.name,
      email: this.email,
      password: this.password,
      image:this.defaultimage
    }


    if ((this.name == '' || this.name == undefined || this.name.length < '3') || (this.password == '' || this.password == undefined || this.password.length < '6')
      || (this.email == '' || this.email == undefined)) {
      this.loading = false;
      Swal.fire({
        title: 'Oops! Please Enter Valid details',
        text: 'Thanks!',
        icon: 'warning',
        confirmButtonText: 'Ok',
      })
    } else {

      this.api.addUsers(data).subscribe(
        (response: any) => {
          var res = response;
          if (res.error == 1) {
            this.messageError = res.message;

            Swal.fire({
              title: 'User already exists',
              text: 'Thanks!',
              icon: 'warning',
              confirmButtonText: 'Ok',
            })
            this.loading = false;

          } else {
            this.messageError = "";
            Swal.fire({
              title: 'You are registered successfully',
              text: 'Thanks!',
              icon: 'success',
              confirmButtonText: 'Ok',
            })
            this.usersArray = response.data;
            this.loading = false;
            this.router.navigate(['/']);
            console.log(data)
          }
        }, (err: any) => {
          console.log(err)
        }
      )
    }

  }

  loginWithGoogle(): void {

    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      // alert(JSON.stringify(this.socialUser));
      let data = {
        name: this.socialUser['name'],
        social_id: this.socialUser['id'],
        email: this.socialUser['email'],
        provider: this.socialUser['provider'],
        image: this.socialUser['photoUrl']
      }
      this.api.socialLogin(data).subscribe(
        (response: any) => {
          var res = response;
          if (res.error == 1) {
            this.messageError = res.message;
          } else {
            this.messageError = "";
            this.router.navigate(['enacteservices.net/tinyprep/'])
            this.router.navigate(['dashboard'])
            localStorage.setItem("token", res.data['social_id'])
          }
        }, (err: any) => {
          console.log(err)
        }
      )
    });

  }
  loginWithFacebook(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
    });
    // loginWithFacebook(): void {
    // this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
    // }
    // this.socialAuthService.authState.subscribe((user) => {
    //   this.socialUser = user;
    //   // alert(JSON.stringify(this.socialUser));
    //   let data = {                       
    //     name: this.socialUser['name'],
    //     social_id: this.socialUser['id'],
    //     email:this.socialUser['email'],
    //     provider:this.socialUser['provider'], 
    //     image:this.socialUser['photoUrl']
    //   }
    //   this.api.socialLogin(data).subscribe(
    //     (response: any) => {
    //       var res = response;
    //     if(res.error == 1){
    //       this.messageError = res.message;
    //     }else{
    //       this.messageError = "";
    //       this.router.navigate(['dashboard'])
    //       localStorage.setItem("token",res.data['social_id'])
    //     }
    //     }, (err: any) => {
    //            console.log(err)
    //     }
    //   )
    // });
  }

  signupListing() {
    throw new Error('Method not implemented.');
  }

}



