import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BackendapiService } from '../services/backendapi.service';
// import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { Location } from '@angular/common'
import { ConditionalExpr } from '@angular/compiler';



@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  productsArray: any = [];
  isButtonVisible:boolean = true;

  id: any;
  name: string | undefined;
  activeCategoryId: any;
  products_name: any;
  products_price: any;
  products_quantity: any;
  products_unit: any;
  products_id: any;
  products_store: any;
  products_total: any = 0;
  subCategories_id: any;
  products_stocks_lasts: any;
  products_shelf_life: any;
  products_expiry_date: any;
  products_notes: any;
  products_image: any;
  store_name: any;
  category_name: any;
  storeArray: any;
  categoriesArray: any;
  productsName: any;
  productsView: any;
  formGroup = new FormGroup({});
  imageDirectoryPath: any = "https://enacteservices.net/tinyprepbackend/";
  selectElementText: any;
  subcategoriesArray: any;
  products_sub_cat_id: any;
  subCategories_name: any;
  loading: boolean = false;
  default_store: any;
  messageError: any;
  storeListsArray: any;
  subcategory: any;
  category: any;
  messageNotFound: any;
  userType: any;
  stores_name: any;
  itemList = [];
  loadingSpinner: boolean = false;
  imageSrc: string | ArrayBuffer | any = "";
  fileChanges: boolean = false;
  cat_it: any = "";
  sub_cat_it: any = "";
  hidetd: any = [];
  activeI: any = "";
  imageSource: string | ArrayBuffer | any = [];
  filesImage: any = [];
  defStoreArray: any;
  productQuantity: any;
  products_search: any = "";
  products_category_id: any;
  productName: any;
  productPrice: any;
  productShelfLife: any;
  productsNotes: any;
  defaultStore: any;
  if_records: boolean = true;
  subcatListsArray: any;
  subArray: any;
  subcategorySel: any;
  subcategorylistData: any;

  filterReset: any = [];
  actualCategory: any;
  actualSubCategory: any;
  actualSubCategories_id: any;
  actualCategory_id: any;

  formattedDate:any="";
  constructor(
    private api: BackendapiService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private _fb: FormBuilder
  ) {
    
    var user = localStorage.getItem('token');
    this.userType = localStorage.getItem('type');
    if (!user) {                            //user is used to restrict page access 
      this.router.navigate(['/']);
    } 

    this.route.queryParams.subscribe(
      (params: { [x: string]: string | undefined }) => {
        this.id = params["id"];
        this.cat_it = this.id;
        this.subCategories_id = params["subCategories_id"];
        this.subCategories_name = this.subCategories_id;
        this.subcategory = params["subcategory"]?.replace("9999", "&");
        this.category = params["category"];
        this.actualCategory = params["category"];
        this.actualSubCategory = params["subcategory"]?.replace("9999", "&");
        this.actualSubCategories_id = params["subCategories_id"];
        this.actualCategory_id = params["id"];
      }
    );

    this.filteredProduct();
    this.categories();
    this.subcategories();
    this.storeLists();
    this.storeListsSearch();
    this.subcatListing();
    this.script_for_sort();
    this.removeUpload();
    this.script_for_all();
    this.selectElementText = this.id;

    
  }

  onDragOver(event: any) {
    event.preventDefault();
  }

  onDropSuccess(event: any) {
    event.preventDefault();

    this.onFileChange(event.dataTransfer.files);
  }

  onChange(event: any) {
    event.preventDefault();
    this.onFileChange(event.target.files);
  }

  private onFileChange(files: File[]) {
    const file = files;
    const reader = new FileReader();
    reader.onload = (e) => (this.imageSrc = reader.result);
    this.fileChanges = true;
    this.selectedFile = file[0];
    this.filesImage = file[0];
    //console.log(this.imageSrc)
    reader.readAsDataURL(files[0]);
    $(".default-upload-image").hide();
    $("button.remove-image").closest(".image-title-wrap").show();
  }

  removeUpload() {
   
    $(document).on("click", "button.remove-image", function () {
      $(this)
        .closest(".file-upload")
        .find(".file-upload-image")
        .removeAttr("src");
      $(this).closest(".file-upload").find(".default-upload-image").show();

      $(this).closest(".image-title-wrap").hide();
    });
  }

  closeProductpopup(
    products_id: any,
    products_name: any,
    products_quantity: any,
    products_shelf_life: any,
    products_notes: any,
    products_image: any,
    i: any
  ) {
    var $this = this;
    setTimeout(function () {
      //  $this.editProduct(products_id, products_name, products_quantity, products_shelf_life, products_notes, products_image, i);

      $(".default-upload-image").hide();
      $("button.remove-image").closest(".image-title-wrap").show();
    }, 1000);
  }

  filteredProduct() {


    this.loadingSpinner = true; //    page load spinner
    var data = {
      subCategories_id: this.subCategories_id,
      id: this.id,
    };

    this.api.filteredProduct(data).subscribe(
      (response: any) => {
        if (JSON.stringify(response.data)) {
         
          this.loadingSpinner = false; //    page load spinner
        }
        this.messageNotFound = "No Record Found";
        this.productsArray = response.data;

       

      },
      (err: any) => {}
    );
  }

  ngOnInit(): void {
    this.formGroup = this._fb.group({
      products_name: ["", [Validators.required, Validators.minLength(3)]],
      productName: ["", [Validators.required, Validators.minLength(3)]],
      products_quantity: ["", [Validators.required, Validators.min(1)]],
      // 'productShelfLife': ['', [Validators.required, Validators.pattern('\\-?\\d*\\.?\\d{1,2}')]],
      productShelfLife: ["", [Validators.required, Validators.min(1)]],
      productQuantity: ["", [Validators.required, Validators.min(1)]],
      cat_it: ["", [Validators.required]],
      subCategories_name: ["", [Validators.required]],
      productPrice: ["", [Validators.required]],
      defaultStore: ["", [Validators.required]],
      productsNotes: ["", [Validators.required]],
      products_shelf_life: ["", [Validators.required]],
      products_notes: ["", [Validators.required]],
      products_image: ["", [Validators.required]],
    });
  }

  viewProduct(products_id: any) {
    this.loadingSpinner = true; //    page load spinner
    var data = {
      products_id: products_id,
    };
    this.api.viewProduct(data).subscribe(
      (response: any) => {
        if (JSON.stringify(response.data)) {
          //  this.hideloader();
          this.loadingSpinner = false; //    page load spinner
        }
        this.productsView = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
    return true;
  }

  deleteProduct(products_id: any) {
    var data = {
      products_id: products_id,
    };
    Swal.fire({
      title: "Are you sure want to delete?",
      text: "You will not be able to recover this product!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, keep it",
    }).then((result) => {
      if (result.value) {
        this.loadingSpinner = true; //    page load spinner
        this.api.deleteProduct(data).subscribe(
          (response: any) => {
            // this.productsArray();
            this.loadingSpinner = true; //    page load spinner
            this.filteredProduct();
            Swal.fire("Deleted!", "Product deleted successfully.", "success");
            // this.productsArray = response.data;
            // Swal.fire({
            //   title: 'Product deleted successfully.',
            //   text: 'Thanks!',
            //   icon: 'success',
            //   showCancelButton: true,
            //   confirmButtonText: 'Ok',
            //   cancelButtonText: 'Cancel'
            // })
          },
          (err: any) => {
            console.log(err);
            Swal.fire({
              title: "Oops! Something went wrong",
              text: "Thanks!",
              icon: "warning",
              //showCancelButton: true,
              confirmButtonText: "Ok",
              //cancelButtonText: "Cancel",
            });
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire("Cancelled", "Your Product is safe :)", "error");
      }
    });
    
  }
  // productsLists() {
  //   var data = {};
  //   data = {
  //     id : this.id
  //   }
  //   this.api.productsLists(data).subscribe(
  //     (response :any) => {
  //       this.productsArray = response.data;
  //     }, (err:any) => {
  //       console.log(err)
  //     }
  //   )
  // }
  // getSelectedOptionsText(event: any) {
  //   let selectedOptions = event.target['options'];
  //   let selectedIndex = selectedOptions.selectedIndex;
  //   this.selectElementText = selectedOptions[selectedIndex].value;

  //   // alert(this.selectElementText);
  //   // alert(this.selectElementText);

  //   // console.log(selectElementText)
  // }
  addProducts() {
    if (
      this.productName == "" ||
      this.productName == undefined ||
      this.productQuantity == "" ||
      this.productQuantity == undefined ||
      this.productShelfLife == undefined ||
      this.productShelfLife == "" ||
      this.selectElementText == undefined ||
      this.selectElementText == "" ||
      this.subCategories_name == undefined ||
      this.subCategories_name == ""
    ) {
      // this.loading = true;
      Swal.fire({
        title: "Oops! Please Enter Valid Details.",
        text: "Thanks!",
        icon: "warning",
        //showCancelButton: true,
        confirmButtonText: "Ok",
       // cancelButtonText: "Cancel",
      });
    } else {
      var total_usage_days = this.productQuantity * this.productShelfLife;
      var someDate = new Date();
      var numberOfDaysToAdd = total_usage_days;
      someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
      var rightNow = new Date(someDate);
      var res = rightNow.toISOString().slice(0, 10).replace(/-/g, "-");

      this.loading = true;
      const fd = new FormData();
      fd.append("products_image", this.selectedFile, this.selectedFile.name);
      fd.append("products_name", this.productName);
      fd.append("products_quantity", this.productQuantity);
      fd.append("products_price", this.productPrice);
      fd.append("products_shelf_life", this.productShelfLife);
      fd.append("products_notes", this.productsNotes);
      fd.append("products_category_id", this.selectElementText);
      fd.append("products_sub_cat_id", this.subCategories_name);
      fd.append("products_stocks_lasts", res);

      if (this.productsNotes == undefined) {
        fd.append("products_notes", "N/A");
      }

      if (this.defaultStore != null) {
        fd.append("default_store", this.defaultStore);
      }

      this.api.addProducts(fd).subscribe(
        (response: any) => {
          var res = response;
          if (res.error == 1) {
            this.loading = false;
            Swal.fire({
              title: "Oops! Please Enter Valid details",
              text: "Thanks!",
              icon: "warning",
              //showCancelButton: true,
              confirmButtonText: "Ok",
              //cancelButtonText: "Cancel",
            });
            this.closeNav();
            this.messageError = res.message;
          } else {
            this.loading = false;

            this.messageError = "";
            if ($("#Congratulations1").length > 0) {
              this.openhooraypopup();
            } else {
              Swal.fire({
                title: "Product Added Successfully",
                text: "You will find product inside category",
                icon: "success",
               // showCancelButton: true,
                confirmButtonText: "Ok",
                //cancelButtonText: "Cancel",
              });
            }

            this.filteredProduct();
            this.productsArray = response.data;

            this.closeNav();
          }
        },
        (err: any) => {
          this.loading = false;
          console.log(err);
          Swal.fire({
            title: "Oops! Please Enter Valid details",
            text: "Thanks!",
            icon: "warning",
            //showCancelButton: true,
            confirmButtonText: "Ok",
            //cancelButtonText: "Cancel",
          });
          this.closeNav();
        }
      );
    }
  }

  searchApi() {
    if ((this.productsName == '') || (this.productsName == undefined)) {
  
      this.filteredProduct();
      Swal.fire({
        title: "Oops! Please Enter Product Name.",
        text: "Thanks!",
        icon: "warning",
        //showCancelButton: true,
        confirmButtonText: "Ok",
        //cancelButtonText: "Cancel",
      });
    } else {
      this.loading = true;
     
      var data = {};
      data = {
        products_name: this.productsName

      }
     
      this.api.searchApi(data).subscribe(
        (response: any) => {
          this.loading = false;
          this.productsArray = response.data;
          
        }, (err: any) => {
          this.loading = false;
          console.log(err);
        }
      );
    }
  }

  storeListsSearch() {
    this.api.storeLists().subscribe(
      (response: any) => {
        this.storeArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }
  
  storeLists() {
    this.api.storeLists().subscribe(
      (response: any) => {
        this.storeListsArray = response.data;
        this.defaultStore = this.storeListsArray.filter(
          (x: any) => x.stores_def == 1
        )[0].stores_id;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  

  getSelectedOptionText(event: any) {
  
    let selectedOptions = event.target["options"];
    let selectedIndex = selectedOptions.selectedIndex;
    this.selectElementText = selectedOptions[selectedIndex].value;

    //  alert(this.selectElementText);
    // this.subcategories();
    //event.target.value
    this.subArray = this.subcategorylistData.filter(
      (x: any) => x.subCategories_cat_id == this.selectElementText
    );
  }

  categories() {
    this.api.categories().subscribe(
      (response: any) => {
        this.categoriesArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  subcategories() {
    this.api.subcategories().subscribe(
      (response: any) => {
        this.subcategoriesArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  subcatListing() {
    this.api.subcatListing().subscribe(
      (response: any) => {
        this.subcategorylistData = this.subArray = response.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  searchProducts() {
    if (
      this.store_name == "" ||
      this.store_name == undefined ||
      this.category_name == "" ||
      this.category_name == undefined
    ) {
      // this.loading = true;
      Swal.fire({
        title: "Oops! Please select category and store name.",
        text: "Thanks!",
        icon: "warning",
        //showCancelButton: true,
        confirmButtonText: "Ok",
        //cancelButtonText: "Cancel",
      });
    } else {
      this.loading = true;
      var data = {};

      data = {
        store_name: this.store_name,
        category_name: this.category_name,
      };

      this.api.searchProducts(data).subscribe(
        (response: any) => {
          this.loading = false;
          this.productsArray = response.data;
          console.log("products array", this.productsArray);
        },
        (err: any) => {
          this.loading = false;
          console.log(err);
        }
      );
    }
  }

  selectedFile: any = null;
  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  filterItems() {
    this.if_records = false;
    var str2 = this.products_search.toLowerCase();
    var arr: any = [];
    this.productsArray.forEach((element: any) => {
      var str1 = element.products_name.toLowerCase();
      arr.push(str1);
      if (str1.indexOf(str2) != -1) {
        this.if_records = true;
        console.log(str2 + " found");
      }
    });
  }

  editProduct(
    products_id: any,
    products_name: any,
    products_quantity: any,
    products_shelf_life: any,
    products_notes: any,
    products_image: any,
    products_price: any,
    i: any
  ) {
    this.products_id = products_id;
    this.products_name = products_name;
    this.products_quantity = products_quantity;
    this.products_shelf_life = products_shelf_life;
    this.products_notes = products_notes;
    this.products_image = products_image;
    this.products_price = products_price;
    this.imageSource[i] = this.imageDirectoryPath + "" + this.products_image;
    this.hidetd[i] = true;
    this.activeI = i;

    for (var $i = 0; $i < this.hidetd.length; $i++) {
      if ($i !== i) this.hidetd[$i] = false;
    }

   
    var cid = $(".open-modal-popup-item").attr("data-target");
    if (cid) {
      $(cid)
        .find(".file-upload .file-upload-image")
        .attr("src", this.imageDirectoryPath + products_image);
    }
  }
  editProducts(i: any, id: any) {
    this.loading = true;

    var total_usage_days =this.productsArray[i].products_quantity * this.productsArray[i].products_shelf_life;
    var someDate = new Date();
    var numberOfDaysToAdd = total_usage_days;
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
    var rightNow = new Date(someDate);
    var res = rightNow.toISOString().slice(0, 10).replace(/-/g, "-");

    console.log("Product name",this.productsArray[i].products_name);

    const fd = new FormData();
    fd.append("products_id", id);
    fd.append("products_name", this.productsArray[i].products_name);
    fd.append("products_quantity", this.productsArray[i].products_quantity);
    fd.append(
      "products_category_id",
      this.productsArray[i].products_category_id
    );
    fd.append("products_sub_cat_id", this.productsArray[i].products_sub_cat_id);
    fd.append("products_store", this.productsArray[i].products_store);
    fd.append("products_shelf_life", this.productsArray[i].products_shelf_life);
    fd.append("products_price", this.productsArray[i].products_price);
    fd.append("products_stocks_lasts", res);
    fd.append("products_notes", this.productsArray[i].products_notes);
   
    
    if (this.filesImage.name) {
      fd.append("products_image", this.filesImage, this.filesImage.name);
    } else {
      fd.append("products_image_n", this.productsArray[i].products_image);
    }

    if (
      this.products_name == "" ||
      this.products_name == undefined ||
      this.products_name.length < "3" ||
      this.products_quantity == "" ||
      this.products_quantity == undefined
    ) {
      this.loading = false;
      if ($("#myModals").hasClass("show")) {
        $(".closepopup").trigger("click");
      }
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        text: "Thanks!",
        icon: "warning",
        //showCancelButton: true,
        confirmButtonText: "Ok",
        //cancelButtonText: "Cancel",
      });
    } else {
      this.api.editProducts(fd).subscribe(
        (response: any) => {
          Swal.fire({
            title: "Product Updated Successfully",
            text: "Thanks!",
            icon: "success",
           // showCancelButton: true,
            confirmButtonText: "Ok",
           // cancelButtonText: "Cancel",
          });
          this.loading = false;
          if ($("#myModals").hasClass("show")) {
            $(".closepopup").trigger("click");
          }
          this.hidetd[i] = false;
          this.loading = false;
          this.filteredProduct();
        },
        (err: any) => {
          Swal.fire({
            title: "Oops! Something went wrong.",
            text: "Thanks!",
            icon: "warning",
            //showCancelButton: true,
            confirmButtonText: "Ok",
            //cancelButtonText: "Cancel",
          });
          this.loading = false;
          if ($("#myModals").hasClass("show")) {
            $(".closepopup").trigger("click");
          }
          console.log(err);
        }
      );
    }
  }
  editItems() {
    this.loading = true;

    var total_usage_days = this.products_quantity * this.products_shelf_life;
    var someDate = new Date();
    var numberOfDaysToAdd = total_usage_days;
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
    var rightNow = new Date(someDate);
    var res = rightNow.toISOString().slice(0, 10).replace(/-/g, "-");

    const fd = new FormData();
    fd.append("products_id", this.products_id);
    fd.append("products_name", this.products_name);
    fd.append("products_quantity", this.products_quantity);
    fd.append("products_stocks_lasts", res);
    fd.append("products_shelf_life", this.products_shelf_life);
    fd.append("products_notes", this.products_notes);
    if (this.filesImage.name) {
      fd.append("products_image", this.selectedFile, this.selectedFile.name);
    } else {
      fd.append("products_image", this.products_image);
    }

    if (
      this.products_name == "" ||
      this.products_name == undefined ||
      this.products_name.length < "3" ||
      this.products_quantity == "" ||
      this.products_quantity == undefined
    ) {
      this.loading = false;
      if ($("#myModals").hasClass("show")) {
        $(".closepopup").trigger("click");
      }
      Swal.fire({
        title: "Oops! Please Enter Valid details",
        text: "Thanks!",
        icon: "warning",
        //showCancelButton: true,
        confirmButtonText: "Ok",
       // cancelButtonText: "Cancel",
      });
    } else {
      this.api.editItems(fd).subscribe(
        (response: any) => {
          Swal.fire({
            title: "Product Updated Successfully",
            text: "Thanks!",
            icon: "success",
           // showCancelButton: true,
            confirmButtonText: "Ok",
           // cancelButtonText: "Cancel",
          });

          this.loading = false;
          if ($("#myModals").hasClass("show")) {
            $(".closepopup").trigger("click");
          }
          this.filteredProduct();
        },
        (err: any) => {
          Swal.fire({
            title: "Oops! Something went wrong.",
            text: "Thanks!",
            icon: "warning",
           // showCancelButton: true,
            confirmButtonText: "Ok",
            //cancelButtonText: "Cancel",
          });
          this.loading = false;
          if ($("#myModals").hasClass("show")) {
            $(".closepopup").trigger("click");
          }
          console.log(err);
        }
      );
    }
  }
  openNav() {
    // (document.getElementById("openModel") as HTMLElement).click();
    if (document.getElementById("mySidebar"))
      (document.getElementById("mySidebar") as HTMLElement).style.width =
        "500px";
    if (document.getElementById("main"))
      (document.getElementById("main") as HTMLElement).style.marginLeft =
        "500px";
  }

  closeNav() {
    // (document.getElementById("openModel") as HTMLElement).click();
    if (document.getElementById("mySidebar"))
      (document.getElementById("mySidebar") as HTMLElement).style.width = "0";
    if (document.getElementById("main"))
      (document.getElementById("main") as HTMLElement).style.marginLeft = "0";
  }

  updateProductImage(event: any, i: any) {
    this.filesImage = [];
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = (e) => (this.imageSource[i] = reader.result);
      this.filesImage = file;
      reader.readAsDataURL(file);
    }
  }

  saveUpdate(index: any, id: any) {
    this.editProducts(index, id);
  }

  // script for sort row
  script_for_sort() {
    $(document).ready(function () {
      $("table.table.inventory thead tr th.th-sort-head").each(function () {
        if (!$(this).find("span").hasClass("sort-head")) {
          $(this).append(
            '<span class="sort-head"><i class="fas fa-arrow-up"></i></span>'
          );
        }
      });
      $("table.table.inventory thead tr th span.sort-head").hide();
      $("table.table.inventory thead tr th span.sort-head")
        .closest("th")
        .css("cursor", "pointer");
      $("table.table.inventory thead tr th span.sort-head")
        .closest("th")
        .mouseover(function () {
          $("table.table.inventory thead tr th span.sort-head").hide();
          $(this).find("span.sort-head").show();
        });

      $("table.table.inventory thead tr th span.sort-head")
        .closest("th")
        .click(function () {
          $("table.table.inventory thead tr th span.sort-head").removeClass(
            "active"
          );
          $(this).find("span.sort-head").addClass("active");
        });

      var f_sl = 1;
      var f_nm = 1;
      $("table.table.inventory thead tr th.th-sort-head").click(function () {
        f_sl *= -1;
        var n = $(this).prevAll().length;
        sortTable(f_sl, n, $(this));
      });
    });

    function sortTable(f: any, n: any, $this: any) {
      var rows = $("#product-list-table tbody  tr").get();

      rows.sort(function (a, b) {
        var A = getVal(a);
        var B = getVal(b);

        if (A < B) {
          $this
            .find("span.sort-head i")
            .removeClass("fa-arrow-down")
            .addClass("fa-arrow-up");
          return -1 * f;
        }
        if (A > B) {
          $this
            .find("span.sort-head i")
            .removeClass("fa-arrow-up")
            .addClass("fa-arrow-down");
          return 1 * f;
        }
        return 0;
      });

      function getVal(elm: any) {
        var v = $(elm).children("td").eq(n).text().toUpperCase();
        // if($.isNumeric(v)){
        // 	v = parseInt(v,10);
        // }
        return v;
      }

      $.each(rows, function (index, row) {
        $("#product-list-table").children("tbody").append(row);
      });
    }
  }

  script_for_all() {
    // hide image preview if image src is empty.
    setInterval(() => {
      $(".file-upload-content .file-upload-image").each(function () {
        if ($(this).attr("src")) {
          $(this).show();
        } else {
          $(this).hide();
        }
      });
    });
    setTimeout(() => {
      var contentforcpopup = localStorage.content_for_cpopup;
      if (contentforcpopup) {
        contentforcpopup = contentforcpopup.split("+");
        if ($("#cuserid").val() == contentforcpopup[1].replace("userid=", "")) {
          $("#" + contentforcpopup[0]).remove();
        }
      }
    }, 3000);

    var $this = this;
    // When the user clicks anywhere outside of the sidebar, close it

    $(document).click(function (e: any) {
      // Get the search popup container.
      var search_popup = $(".serch-popup-container")[0];
      // Get the sidebar
      var sidebar = document.getElementById("mySidebar");
      // close sidebar popup when click outside.
      if (sidebar) {
        let offsetWidth = sidebar.offsetWidth;
        var mysidebar = $(e.target).closest("#mySidebar").attr("id");
        if (offsetWidth > 0) {
          if (mysidebar != "mySidebar") {
            (document.getElementById("mySidebar") as HTMLElement).style.width =
              "0";
          }
        }
      }

      // close search filter popup when click outside
      if (search_popup) {
        let offsetWidth_serchpopup = search_popup.offsetHeight;
        var myserch_popup = $(e.target).closest(".serch-popup-container");
        if (offsetWidth_serchpopup > 2)
          if (!myserch_popup.hasClass("serch-popup-container")) {
            $(".search.search-filter-popup .serch-popup-container").slideUp(
              "fast"
            );
          }
      }
    });
    // hide other row when open current row
    $(document).on("click", ".nav-link,.btn.items.openbtn", function () {
      for (var $i = 0; $i < $this.hidetd.length; $i++) {
        $this.hidetd[$i] = false;
      }

      // selected category and subcategory.
      setTimeout(() => {
        $("#mySidebar .remove-image").trigger("click");
        let params = new URLSearchParams(document.location.search.substring(1));
        let cat_id = params.get("id");

        let subCategories_id = params.get("subCategories_id");
        $this.subCategories_id = subCategories_id;
        $this.id = cat_id;
        $this.cat_it = cat_id;
        $this.subCategories_name = subCategories_id;
        $this.defaultStore = 1;
      });
    });
  }

  // toggle search filter popup.
  toggle_search_filter_popup() {
    $(".search.search-filter-popup .serch-popup-container").slideToggle();
  }

  cancelsearchfilterpopup() {
    $(".search.search-filter-popup .serch-popup-container").slideUp("fast");
  }

  applysearchfilterpopup(e: any) {
    let form_data = $(e.target).closest("form").serialize();

    console.log(form_data);

    $(".navbarr-Sub-category h6 a.active").text(
      $('select[name="subcat_id"] :selected').text()
    );
    $("#main-category-list").text($('select[name="cat_id"] :selected').text());
    window.history.pushState(
      "",
      "list",
      "/list?subCategories_id=" +
        $('select[name="subcat_id"] :selected').val() +
        "&id=" +
        $('select[name="cat_id"] :selected').val() +
        "&category=" +
        $('select[name="cat_id"] :selected').text() +
        "&subcategory=" +
        ($('select[name="subcat_id"] :selected').text()
          ? $('select[name="subcat_id"] :selected').text().replace("9999", "")
          : "")
    );
    //this.id = $('select[name="cat_id"] :selected').val()
    this.category = $('select[name="cat_id"] :selected').text();
    //this.subCategories_id = $('select[name="subcat_id"] :selected').val();

    this.loading = true;
    var data = form_data;

    //console.log(data);

    this.api.searchProductsfrompopup(data).subscribe(
      (response: any) => {
        this.loading = false;
        if (response) {
          this.if_records = true;
        } else {
          this.if_records = false;
        }
        this.productsArray = response;

        console.log("products array", this.productsArray);
        this.filterReset = this.productsArray.length;
        //this.storeLists();
        this.cancelsearchfilterpopup();
      },
      (err: any) => {
        this.loading = false;
        console.log(err);
      }
    );
  }

  resetFilterBtn() {
    this.productsArray = [];
    this.category = "";
    this.subcategory = "";
    this.subCategories_id = "";
    this.cat_it = "";
    this.category = this.actualCategory;
    this.subcategory = this.actualSubCategory;
    this.subCategories_id = this.actualSubCategories_id;
    this.cat_it = this.actualCategory_id;
    $("#main-category-list").text(this.category);
    $(".navbarr-Sub-category h6 a.active").text(this.subcategory);
    window.history.pushState(
      "",
      "list",
      "/list?subCategories_id=" +
        this.subCategories_id +
        "&id=" +
        this.cat_it +
        "&category=" +
        this.category +
        "&subcategory=" +
        this.subcategory
    );

    this.filteredProduct();
    this.isButtonVisible = false;
  }

  openhooraypopup() {
    $(".Congratulations1-action").trigger("click");
    $("body").addClass("modal-open");
  }

  finish_shopping() {
    $("#Congratulations1")
      .removeClass("show")
      .removeAttr("style")
      .attr("aria-hidden", "true");
    $("body").removeClass("modal-open");
    $(".modal-backdrop").remove();

    $(".shopping-go-action").trigger("click");
    $("body").addClass("modal-open");
  }

  continue_shopping() {
    $("#Congratulations1")
      .removeClass("show")
      .removeAttr("style")
      .attr("aria-hidden", "true");
    $("body").removeClass("modal-open");
    $(".modal-backdrop").remove();

    $(".btn.items.openbtn").trigger("click");
  }

  delete_cpopup() {
    localStorage.content_for_cpopup =
      "Congratulations1+userid=" + $("#cuserid").val();
    $("#Congratulations1")
      .removeClass("show")
      .removeAttr("style")
      .attr("aria-hidden", "true");
    $(".modal-backdrop").remove();

    $("#Congratulations1").remove();
    // $('.btn.items.openbtn').trigger('click');
  }

  go_to_shoppingList() {
    $("#Shopping-go")
      .removeClass("show")
      .removeAttr("style")
      .attr("aria-hidden", "true");
    $("body").removeClass("modal-open");
    $(".modal-backdrop").remove();
    $(".dashboard-left-part ul li#shoppingList").trigger("click");
  }
}
