import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendapiService } from '../services/backendapi.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userDataArray: any;
  cuserDataArray: any;
  name: any;
  defaultimage: any="http://i.pravatar.cc/500?img=7";
  imageDirectoryPath: any = 'https://enacteservices.net/tinyprepbackend/';
  constructor(private api: BackendapiService, private router: Router, private http: HttpClient, private route: ActivatedRoute,private _fb: FormBuilder) {



  //   var user = localStorage.getItem('token');
  //  // var userType = localStorage.getItem('type');  

  //   if (!user) {                            //user is used to restrict page access 
  //      this.router.navigate(['/']);
  //   } 

   }
   

  ngOnInit(): void {
   
    this.userData();
     
  }
 
  
  userData() {
    $('.header-name-right').hide();
    this.api.userData().subscribe(
      (response: any) => {
        var $this = this;
        
         this.userDataArray = response.data[0];
         if(this.userDataArray)
         $('.header-name-right').show();
      }, (err: any) => {
        console.log(err)
      }
    )
  }

  Logout() {
    localStorage.removeItem("token")
    localStorage.removeItem("type")
    localStorage.removeItem('users_id');
    this.router.navigate(['/']);
    this.userDataArray = '';
    $('.header-name-right').hide();
  }
}
