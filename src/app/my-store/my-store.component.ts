import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BackendapiService } from '../services/backendapi.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-my-store',
  templateUrl: './my-store.component.html',
  styleUrls: ['./my-store.component.css']
})
export class MyStoreComponent implements OnInit {

  storeListsArray: any;
  stores_image: any;
  stores_name: any;
  stores_link: any;
  stores_def: any;
  messageError: any;
  imageDirectoryPath: any = 'https://enacteservices.net/tinyprepbackend/';
  stores_id: any;

  myForm: any;
  searchName: any;
  storesearchListsArray: any;
  formGroup = new FormGroup({});
  loading: boolean = false;
  store_default_value: any;

  stores_fav: any;
  messageNotFound: any;

  imageSrc: string | ArrayBuffer | any = "";
  fileChanges: boolean = false;
  filesImage: any = [];
  constructor(private api: BackendapiService, private router: Router, private route: ActivatedRoute, private http: HttpClient,
    private _fb: FormBuilder) {
    this.storeLists();
    this.removeUpload();

  }

  ngOnInit(): void {
    this.formGroup = this._fb.group({
      'stores_name': ['', [Validators.required, Validators.minLength(3)]],
      'stores_image': ['', [Validators.required]],
      'stores_link': ['', [Validators.required, Validators.minLength(7)]]
    })
  }


  removeUpload() {
    $(document).on("click", "button.remove-image", function () {
      $(this)
        .closest(".file-upload")
        .find(".file-upload-image")
        .removeAttr("src");
      $(this).closest(".file-upload").find(".default-upload-image").show();

      $(this).closest(".image-title-wrap").hide();
    });
  }

  storeLists() {
    this.api.storeLists().subscribe(
      (response: any) => {
        this.messageNotFound = 'No Record Found';
        this.storeListsArray = response.data;
      }, (err: any) => {
        console.log(err)
      }
    )
  }


  selectedFile: any = null;
  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }
  searchApi() {
    this.loading = true;
    var data = {};
    data = {
      stores_name: this.searchName

    }

    this.api.searchApi(data).subscribe(
      (response: any) => {
        this.loading = false;
        this.messageNotFound = 'No Record Found';
        if (response) {
          this.storeListsArray = response.data;
        }
        //this.storeLists();
      }, (err: any) => {
        this.loading = false;
        console.log(err)
      }
    )
  }
  checked_val() {
    this.stores_def = !this.stores_def;
  }
  addStores() {
    this.loading = true;
    if (this.stores_def) {
      this.store_default_value = '1';

    } else {
      this.store_default_value = '0';
    }

    const fd = new FormData();
    fd.append('stores_image', this.selectedFile, this.selectedFile.name)
    fd.append('stores_name', this.stores_name)
    fd.append('stores_link', this.stores_link)
    fd.append('stores_def', this.store_default_value)
  
    if ((this.stores_name == '' || this.stores_name == undefined || this.stores_name.length < '3') || (this.stores_link == '' || this.stores_link == undefined || this.stores_link.length < '5')) {
      this.loading = false;
      Swal.fire({
        title: 'Oops! Please Enter Valid details',
        text: 'Thanks!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    }else {

      this.api.addStores(fd).subscribe(
        (response: any) => {
          var res = response;
          if (res.error == 1) {
            this.loading = false;
            Swal.fire({
              title: res.message,
              text: 'Thanks!',
              icon: 'warning',
              confirmButtonText: 'Ok',
              
            })
            this.messageError = res.message;
          } else {
            this.messageError = "";
            this.storeLists();
            this.loading = false;
            Swal.fire({
              title: 'Store Added Successfully',
              text: 'Thanks!',
              icon: 'success',
              confirmButtonText: 'Ok',
            })
          }
        }, (err: any) => {
          this.loading = false;

          Swal.fire({
            title: 'Oops! Please Enter Valid details',
            text: 'Thanks!',
            icon: 'warning',
            confirmButtonText: 'Ok',
          })
          console.log(err)
        }
      )
    }

  }

  onDragOver(event: any) {
    event.preventDefault();
  }


  onDropSuccess(event: any) {
    event.preventDefault();
    this.onFileChange(event.dataTransfer.files);
  }
  onChange(event: any) {
    event.preventDefault();
    this.onFileChange(event.target.files);
  }

  // private onFileChange(files: File[]) {
  //   const file = files;
  //   const reader = new FileReader();
  //   reader.onload = e => this.imageSrc = reader.result;
  //   this.fileChanges = true;
  //   this.selectedFile = file[0]
  //   console.log(this.imageSrc)
  //   reader.readAsDataURL(files[0]);
  // }

  private onFileChange(files: File[]) {
    const file = files;
    const reader = new FileReader();
    reader.onload = (e) => (this.imageSrc = reader.result);
    this.fileChanges = true;
    this.selectedFile = file[0];
    this.filesImage = file[0];
    reader.readAsDataURL(files[0]);
    $(".default-upload-image").hide();
    $("button.remove-image").closest(".image-title-wrap").show();
  }



  editStore(stores_id: any, stores_name: any, stores_link: any, stores_image: any) {
    this.stores_id = stores_id;
    this.stores_name = stores_name;
    this.stores_link = stores_link;
    this.stores_image = stores_image;
  }

  editStores() {

    this.loading = true;
    const fd = new FormData();
    fd.append('stores_image', this.selectedFile, this.selectedFile.name)
    fd.append('stores_name', this.stores_name)
    fd.append('stores_link', this.stores_link)
    fd.append('stores_id', this.stores_id)

    if (
        this.stores_name == '' || this.stores_name == undefined ||  
        this.stores_link == '' || this.stores_link == undefined 
        ) {

        this.loading = false;
        Swal.fire({
          title: 'Oops! Please Enter Valid details',
          text: 'Thanks!',
          icon: 'warning',
          confirmButtonText: 'Ok',
        })
    } else {

      this.api.editStores(fd).subscribe(
        (response: any) => {
          var res = response;
          if (res.error == 1) {
            this.loading = false;
            Swal.fire({
              title: 'Oops! Please Enter Valid details',
              text: 'Thanks!',
              icon: 'warning',
              confirmButtonText: 'Ok'
            })
            this.messageError = res.message;
          } else {
            this.messageError = "";
            this.storeLists();
            this.loading = false;
            Swal.fire({
              title: 'Store Updated Successfully',
              text: 'Thanks!',
              icon: 'success',
              confirmButtonText: 'Ok'
              
            })
          }
        }, (err: any) => {
          console.log(err)
          this.loading = false;
          Swal.fire({
            title: 'Oops! Please Enter Valid details',
            text: 'Thanks!',
            icon: 'warning',
            confirmButtonText: 'Ok',
            
          })
        }
      )
    }
  }


  addToFavorite(stores_id: any) {
    this.stores_fav = !this.stores_fav;

    // alert(JSON.stringify(store));
    if (this.stores_fav) {
      var fav_store = '1';
      Swal.fire({
        title: 'Added To Favorite.',
        text: 'Thanks!',
        icon: 'success',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    } else {
      var fav_store = '0';
      Swal.fire({
        title: 'Removed from Favorite.',
        text: 'Thanks!',
        icon: 'success',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    }
    var data = {}
    data = {
      stores_fav: fav_store,
      stores_id: stores_id
    }
    // alert(JSON.stringify(data));
    this.api.addToFavorite(data).subscribe(
      (response: any) => {
        this.storeLists();
      }, (err: any) => {
        console.log(err)
      }
    )
  }

  deleteStore(stores_id: any) {
    var data = {
      stores_id: stores_id
    }
    var x = confirm("Are you sure you want to delete this store?");
    if (x) {
      this.api.deleteStore(data).subscribe(
        (response: any) => {
          this.storeLists();
          Swal.fire({
            title: 'Store Deleted Successfully',
            text: 'Thanks!',
            icon: 'success',
            confirmButtonText: 'Ok',
          })
        }, (err: any) => {
          Swal.fire({
            title: 'Oops! Something went wrong',
            text: 'Thanks!',
            icon: 'warning',
            confirmButtonText: 'Ok',
          })
          console.log(err)
        }

      )
      return true;
    } else {
      return false;
    }

  }
  openNav() {
    // (document.getElementById("openModel") as HTMLElement).click();
    if (document.getElementById("mySidebar"))
      (document.getElementById("mySidebar") as HTMLElement).style.width = "500px";
    if (document.getElementById("main"))
      (document.getElementById("main") as HTMLElement).style.marginLeft = "500px";
  }

  closeNav() {
    // (document.getElementById("openModel") as HTMLElement).click();
    if (document.getElementById("mySidebar"))
      (document.getElementById("mySidebar") as HTMLElement).style.width = "0";
    if (document.getElementById("main"))
      (document.getElementById("main") as HTMLElement).style.marginLeft = "0";
  }
}
