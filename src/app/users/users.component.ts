import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { BackendapiService } from '../services/backendapi.service';
// import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  userListsArray: any;
  email: any;
  name: any;
  id: any;
  messageError: any;
  errors:any;
  password: any;
  type: any;
  search_name:any;
  formGroup= new FormGroup({});
  loading: boolean = false;
  messageNotFound: any;
  // pipe = new DatePipe('en-US');
  // now = Date.now();
  // mySimpleFormat = this.pipe.transform(this.now, 'MM/dd/yyyy');
  // myShortFormat = this.pipe.transform(this.now, 'short');

  constructor(private api: BackendapiService, private router: Router, private route: ActivatedRoute,
    private _fb: FormBuilder) {
    this.userLists();
    var user = localStorage.getItem('token');
    var userType = localStorage.getItem('type');  
  
    if (user && (userType == '1')) {                              //user is used to restrict page access 
      // this.router.navigate(['/list']);
      this.userLists();
    } else {
      // No user is signed in.
      this.router.navigate(['/']);
    }

   }
   
  ngOnInit(): void {
    this.formGroup = this._fb.group({
       'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
       'name': ['',[Validators.required,Validators.minLength(3)]],
       'password': ['',[Validators.required,Validators.minLength(7)]]
    })
  }
  userLists() {
    // alert('hello');
    // this.router.navigate(['/shoppingLists']);
    this.api.userLists().subscribe(
      (response: any) => {
        this.messageNotFound ='No Record Found';
        this.userListsArray = response.data;
      }, (err: any) => {
        console.log(err)
      }
    )
  }
  searchApi() {
    this.loading = true;
    // debugger
    var data = {};
      data = {
        name: this.search_name
      }
    //const fd = new FormData();
    //fd.append('products_name',this.products_name) 
        this.api.searchApi(data).subscribe(
          (response: any) => {
            this.loading = false;
            this.messageNotFound ='No Record Found';
            this.userListsArray = response.data;
            //this.storeLists();
          }, (err: any) => {
            this.loading = false;
            console.log(err)
          }
        )
    }
  editUser(id: any, name: any, email: any) {
    this.id = id;
    this.name = name;
    this.email = email;
  }
  

  editUsers() {
    this.loading = true;
    var data = {}
    data = {
      name: this.name,
      email: this.email,
      id: this.id
    }
    if ((this.name == '' || this.name == undefined || this.name.length < '3' ) || (this.email == '' || this.email == undefined ||this.email.length < '5')){
     
      Swal.fire({
        title: 'Oops! Please Enter Valid details',
        text: 'Thanks!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    }else{
      
      this.api.editUsers(data).subscribe(
        (response: any) => {
          var res = response;
          if(res.error == 1){
            Swal.fire({
              title: 'Oops! Please Enter Valid details',
              text: 'Thanks!',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Ok',
              cancelButtonText: 'Cancel'
            }) 
            this.loading = false;
            this.messageError = res.message;
          }else{
            this.messageError = "";
          this.loading = false;
          this.userLists();
          Swal.fire({
            title: 'User Updated Successfully',
            text: 'Thanks!',
            icon: 'success',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })
        }
      }, (err: any) => {
          this.loading = false;
          Swal.fire({
            title: 'Oops! Something went wrong',
            text: 'Thanks!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
          })
          console.log(err)
        }
      )
    }
    // if (data != '') {
    //   Swal.fire({
    //     title: 'User Updated Successfully',
    //     text: 'Thanks!',
    //     icon: 'success',
    //     showCancelButton: true,
    //     confirmButtonText: 'Ok',
    //     cancelButtonText: 'Cancel'
    //   })
    // } else {
    //   Swal.fire({
    //     title: 'Oops! Please try after sometime',
    //     text: 'Thanks!',
    //     icon: 'warning',
    //     showCancelButton: true,
    //     confirmButtonText: 'Ok',
    //     cancelButtonText: 'Cancel'
    //   })
    // }
  
  }
  addUsers() {
    // alert('inn');
    this.loading = true;
    var data = {};
    data = {
      name: this.name,
      email: this.email,
      type: 2,
      password: this.password
    }

    // alert(JSON.stringify(this.name.length));
    // alert(JSON.stringify(this.email));
    // alert(JSON.stringify(this.password));
    if ((this.name == '' || this.name == undefined || this.name.length < '3' ) || (this.email == '' || this.email == undefined ||this.email.length < '5') || (this.password == '' || this.password.length < '7' || this.password == undefined)){
      this.loading = false;
      Swal.fire({
        title: 'Oops! Please Enter Valid details',
        text: 'Thanks!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      })
    }else{
    
          this.api.addUsers(data).subscribe(
            (response: any) => {
              // console.log(response);
              // return;
              var res = response;
              if (res.error == 1) {
                this.loading = false;
                Swal.fire({
                  title: 'Oops! Please Enter Valid details',
                  text: 'Thanks!',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Ok',
                  cancelButtonText: 'Cancel'
                })
                this.messageError = res.message;
              } else {
                this.messageError = "";
                this.userLists = response.data;
                Swal.fire({
                  title: 'You are registered successfully',
                  text: 'Thanks!',
                  icon: 'success',
                  showCancelButton: true,
                  confirmButtonText: 'Ok',
                  cancelButtonText: 'Cancel'
                })
                this.loading = false;
                console.log(data)
              }
            }, (err: any) => {

              // console.log(err);
              // return;
              this.loading = false;
              Swal.fire({
                title: 'Oops! Something went wrong',
                text: 'Thanks!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ok',
                cancelButtonText: 'Cancel'
              })
              // console.log(err)
            this.messageError = "";

              this.errors=err.error;
            }
          )
    }
    // if (data == '') {
    //   Swal.fire({
    //     title: 'You are registered successfully',
    //     text: 'Thanks!',
    //     icon: 'success',
    //     showCancelButton: true,
    //     confirmButtonText: 'Ok',
    //     cancelButtonText: 'Cancel'
    //   })
    // } else {
    //   Swal.fire({
    //     title: 'Oops! Please try after sometimes',
    //     text: 'Thanks!',
    //     icon: 'warning',
    //     showCancelButton: true,
    //     confirmButtonText: 'Ok',
    //     cancelButtonText: 'Cancel'
    //   })
    // }

 

  }
      deleteUser(id:any){
          var data = {
            id:id
          }
          var x = confirm("Are you sure you want to delete this user?");
          if(x){
            this.api.deleteUser(data).subscribe(
              (response:any) => {
                Swal.fire({
                  title: 'User deleted successfully!',
                  text: 'Thanks!',
                  icon: 'success',
                  showCancelButton: true,
                  confirmButtonText: 'Ok',
                  cancelButtonText: 'Cancel'
                })
                this.userLists();
              },(err:any) => {
                Swal.fire({
                  title: 'Oops! Please Enter Valid details',
                  text: 'Thanks!',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Ok',
                  cancelButtonText: 'Cancel'
                })
                console.log(err)
              }
  
            )
            return true;
          }else{
          return false;
          }
        
      }

}
