import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendapiService } from 'src/app/services/backendapi.service';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  new_password:any;
  confirm_password:any;
  old_password: any;
  resArray: any;
  userDataArray: any;
  formGroup= new FormGroup({});
  loading: boolean = false;
  constructor(private api: BackendapiService,private router: Router,private _fb: FormBuilder) {

    var user = localStorage.getItem('token');
    // var userType = localStorage.getItem('type');  

    if(!user){                            //user is used to restrict page access 
      this.router.navigate(['/']);
    } 



   }

  ngOnInit(): void {
    this.formGroup = this._fb.group({
      //  'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
       'old_password': ['',[Validators.required]],
       'new_password': ['',[Validators.required,Validators.minLength(6)]],
       'confirm_password': ['',[Validators.required,Validators.minLength(6)]]
    })
  }

  changePassword() {
    this.loading = true;
    // debugger
    // let password = document.getElementById("txtPassword").value;
    //     var confirmPassword = document.getElementById("txtConfirmPassword").value;
    //     if (password != confirmPassword) {
    //         alert("Passwords do not match.");
    //         return false;
    //     }
       if(this.new_password != this.confirm_password){
        Swal.fire({
          title: 'Oops! Password did not match',
          text: 'Thanks!',
          icon: 'warning',
          //showCancelButton: true,
          confirmButtonText: 'Ok',
          //cancelButtonText: 'Cancel'
        })
       }else{
        var data = {};
        data = {
          old_password: this.old_password,
          new_password: this.new_password,
          confirm_password: this.confirm_password
        }
        if ((this.old_password == '' || this.old_password == undefined || this.old_password.length < '6' ) || (this.new_password == '' || this.new_password == undefined || this.new_password.length < '6')
        || (this.confirm_password == '' || this.confirm_password == undefined || this.confirm_password.length < '6')){
          this.loading = false;
          Swal.fire({
            title: 'Oops! Please Enter Valid details',
            text: 'Thanks!',
            icon: 'warning',
            //showCancelButton: true,
            confirmButtonText: 'Ok',
            //cancelButtonText: 'Cancel'
          })   
          
        }else{  
          this.api.changePassword(data).subscribe(
            (response: any) => {
              var res = response;
              this.loading = false;
              if (res.error == 1) {
                Swal.fire({
                  title: 'Oops! Please Enter Valid details',
                  text: 'Thanks!',
                  icon: 'warning',
                 // showCancelButton: true,
                  confirmButtonText: 'Ok',
                //  cancelButtonText: 'Cancel'
                })
              }else{
                this.loading = false;
                this.resArray = response.data;
                Swal.fire({
                  title: 'Password Changed successfully',
                  text: 'Thanks!',
                  icon: 'success',
                 // showCancelButton: true,
                  confirmButtonText: 'Ok',
                 // cancelButtonText: 'Cancel'
                })
              }
          
            }, (err: any) => {
              this.loading = false;
              console.log(err)
            }
          )
        }
       }
  
    }

 

}
