import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BackendapiService } from '../services/backendapi.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpHeaders } from '@angular/common/http';

@Component({
	selector: 'app-items',
	templateUrl: './items.component.html',
	styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
	categoresArray: any = [];
	currentUserSubject: any;
	alertify: any;
	Category: string | undefined;
	route: any;
	category: any;
	ShowSubmitLoader: boolean = false;
	activeCategoryId: any = "";
	users_id: any;
	shoppingListsArray: any;
	categories_name: any;
	categories_image: any;
	categories_notes: any;
	imageDirectoryPath: any = 'https://enacteservices.net/tinyprepbackend/';
	formGroup = new FormGroup({});
	firstName: any;
	loading: boolean = false;
	lastName: any;
	category_search: any = "";
	fd = new FormData();
	loadingSpinner: boolean = false;
	//private _authService: any;
	constructor(usernameElement: ElementRef, private api: BackendapiService, private router: Router, private _fb: FormBuilder, private http: HttpClient) {
		
	var user = localStorage.getItem('token');
      if (!user) {                            //user is used to restrict page access 
        this.router.navigate(['/']);
      } 

	}

	ngOnInit(): void {
		this.categoriesListing();
		this.formGroup = this._fb.group({
			//  'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
			'categories_name': ['', [Validators.required, Validators.minLength(3)]],
			'categories_image': ['', [Validators.required]],
			'categories_notes': ['', [Validators.required, Validators.minLength(4)]],
			'category': ['', [Validators.required, Validators.minLength(3)]]

		})

	}


	editCategory(id: any, cat_name: any) {
		this.activeCategoryId = id
		this.category = cat_name;
	}

	editCategories() {

		var data = {}
		data = {
			name: this.category,
			id: this.activeCategoryId
		}
		if (data != '') {
			Swal.fire({
				title: 'Category Updated Successfully',
				text: 'Thanks!',
				icon: 'success',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				cancelButtonText: 'Cancel'
			})
		} else {
			Swal.fire({
				title: 'Oops! Please try after sometime',
				text: 'Thanks!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				cancelButtonText: 'Cancel'
			})
		}
		this.api.editCategories(data).subscribe(
			(response: any) => {
				this.categoriesListing();
			}, (err: any) => {
				console.log(err)
			}
		)
	}
	//   hideloader() {
	//        alert('inside ');  
	//       //  display: none !important;
	//       debugger
	//       (document.getElementById("loadingSpinner") as HTMLElement).style.display = 'none !important';
	//     // (<HTMLInputElement>document.getElementById('loadingSpinner')).style.display = 'none !important';
	// }
	
	categoriesListing() {
		this.loadingSpinner = true;   //    page load spinner 
		this.api.categoriesListing().subscribe(
			(response: any) => {

				if (JSON.stringify(response.data)) {
					this.loadingSpinner = false;  //    page load spinner 
				}
				this.categoresArray = response.data;
				 console.log("this.categoresArray",this.categoresArray)
			}, (err: any) => {
				console.log(err)
			}
		)
	}

	searchApi() {
		
		if ((this.category_search == '') || (this.category_search == undefined)) {
		

			this.categoriesListing();
			
			Swal.fire({
				title: 'Oops! Please Enter inventory.',
				text: 'Thanks!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				cancelButtonText: 'Cancel'
			})
		} else {
			this.loading = true;
			// debugger
			var data = {};
			data = {
				categories_name: this.category_search

			}
			//const fd = new FormData();
			//fd.append('products_name',this.products_name) 
			this.api.searchApi(data).subscribe(
				(response: any) => {
					this.loading = false;
					this.categoresArray = response.data;
					//this.storeLists();
				}, (err: any) => {
					this.loading = false;
					console.log(err)
				}
			)
		}
	}

	RedirectToShoppingLists() {
		this.router.navigate(['/shoppingLists']);
	}

	selectedFile: any = null;
	onFileSelected(event: any) {
		///let fileList: FileList = event.target.files;
		// let file: File = fileList[0];
		// this.fd.append('uploadFile', file, file.name);
		this.selectedFile = event.target.files[0];
	}
	addCategory() {

		// const fd = new FormData();
		this.fd.append('categories_image', this.selectedFile, this.selectedFile.name)
		this.fd.append('categories_name', this.categories_name)
		this.fd.append('categories_notes', this.categories_notes)

		if ((this.categories_name == '' || this.categories_name == undefined || this.categories_name.length < '3') || (this.categories_notes == '' || this.categories_notes == undefined || this.categories_notes.length < '4')) {

			Swal.fire({
				title: 'Oops! Please Enter Valid details',
				text: 'Thanks!',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				cancelButtonText: 'Cancel'
			})
		}
		else {

			this.api.addCategory(this.fd).subscribe(
				(response: any) => {
					Swal.fire({
						title: 'Category Added Successfully',
						text: 'Thanks!',
						icon: 'success',
						showCancelButton: true,
						confirmButtonText: 'Ok',
						cancelButtonText: 'Cancel'
					})
					this.categoriesListing();
				}, (err: any) => {
					Swal.fire({
						title: 'Oops! Something went wrong,Please try again',
						text: 'Thanks!',
						icon: 'warning',
						showCancelButton: true,
						confirmButtonText: 'Ok',
						cancelButtonText: 'Cancel'
					})
					console.log(err)
				}
			)
		}

	}

	deleteCategoryLists(id: any) {

		var data = {
			id: id
		}

		var x = confirm("Are you sure you want to delete?");
		if (x) {
			this.api.deleteCategoryLists(data).subscribe(
				(response: any) => {
					this.categoriesListing();
				}, (err: any) => {
					console.log(err)
				}
			)
			return true;
		} else {
			return false;
		}

	}
	
	// Logout() {
	//   localStorage.removeItem("token")
	//   localStorage.removeItem("type")
	//   localStorage.removeItem('users_id');
	//   this.router.navigate(['/']);
	// }

	filterCat(){
		var category_search = this.category_search;
		console.log("Food".indexOf(category_search));
	}

}
function redirect(arg0: string) {
	throw new Error('Function not implemented.');
}

