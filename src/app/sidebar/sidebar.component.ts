import { Component, OnInit } from '@angular/core';
import { BackendapiService } from '../services/backendapi.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  type: any;
  routerurl: any;
  // menuList=[{name:"dashboard",image:"assets/theme/images/sidebar-icon1.png",selected:true,routerPath:"/dashboard"},
  // {name:"inventory", image:"assets/theme/images/sidebar-icon2.png",selected:false,routerPath:"/category-list"},
  // {name:"shoppingList",image:"assets/theme/images/sidebar-icon3.png",selected:false,routerPath:"/shoppingLists"},
  // {name:"myStore",image:"assets/theme/images/sidebar-icon4.png",selected:false,routerPath:"/dashboard"},
  // {name:"course",image:"assets/theme/images/sidebar-icon5.png",selected:false,routerPath:"/dashboard"}] //array
  constructor(private api: BackendapiService, private router: Router, private _fb: FormBuilder,
    private route:ActivatedRoute) {
       this.type = localStorage.getItem("type");

     }

  ngOnInit(): void {

    this.routerurl =  this.router.url.split('?')[0];
    // console.log("this",this.menuList)
  }
  
  // menuActive(valSelected:any){
  //   //debugger
  //   alert('ooooo');
  //   this.menuList.forEach((item,index)=>{
  //     if(index == valSelected){
  //       item.selected = true;
  //       this.router.navigate([item.routerPath],{relativeTo:this.route});
  //       //window.location.href = item.routerPath;
      
         
  //     }else{
  //         item.selected = false;
  //       }
  //   })
   
  //   // this.menuList.forEach((x,i)=>{
  //   //   if(i == valSelected){
  //   //     this.menuList[valSelected].selected=true;
  //   //   }else{
  //   //     this.menuList[valSelected].selected=false;
  //   //   }
  //   // })

  
  // }
}
