import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendapiService } from 'src/app/services/backendapi.service';

@Component({
  selector: 'app-get-help',
  templateUrl: './get-help.component.html',
  styleUrls: ['./get-help.component.css']
})
export class GetHelpComponent implements OnInit {

  constructor(private api: BackendapiService,private router: Router) {
    var user = localStorage.getItem('token');
    // var userType = localStorage.getItem('type');  

    if (user) {                            //user is used to restrict page access 
      // this.router.navigate(['/dashboard']);
    } else {
      // No user is signed in.
      this.router.navigate(['/']);
    }

   }

  ngOnInit(): void {
    
  }

}
