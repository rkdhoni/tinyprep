import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TinyPrepCourseComponent } from './tiny-prep-course.component';

describe('TinyPrepCourseComponent', () => {
  let component: TinyPrepCourseComponent;
  let fixture: ComponentFixture<TinyPrepCourseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TinyPrepCourseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TinyPrepCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
