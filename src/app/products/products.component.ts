import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendapiService } from '../services/backendapi.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';




@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products_name: any = "";
  products_category_id: any = "";
  productsArray:any[]= [];
  name: string | undefined;
  id: any;
  products_quantity: any;
  products_unit: any;
  products_price: any;
  products_store: any;
  products_fav_store:any;
  defaultStoreArray: any;
  storeListsArray: any;
  default_store: any;
  products_shelf_life:any;
  products_stocks_lasts:any;
  products_notes:any;
  products_expiry_date:any;
  products_image: any;
  formGroup= new FormGroup({});
  products_sub_cat_id: any;
  subcategoryListsArray: any;
  selectElementText: any;
  messageError: any;
  loading: boolean = false;
  //activeCategoryId: string | undefined;
  //api: any;
  constructor(private api: BackendapiService, private router: Router, private http: HttpClient, private route: ActivatedRoute,private _fb: FormBuilder) {
    this.storeLists();
    this.subcategoryLists();
    var user = localStorage.getItem('token');
    var userType = localStorage.getItem('type');  
  
    // if (user && (userType == '2')) {                              //user is used to restrict page access 
    //   //this.router.navigate(['/products']);
    // } else {
    //   // No user is signed in.
    //   this.router.navigate(['/']);
    // }
    this.route.queryParams.subscribe((params: { [x: string]: string | undefined; }) => {
      this.id = params['id'];
      this.name = params['name'];
    });
  }

  ngOnInit(): void {
    this.favoriteLists();
    this.formGroup = this._fb.group({
      //  'email': ['',[Validators.required,Validators.email,Validators.maxLength(10), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
       'products_name': ['',[Validators.required,Validators.minLength(3)]],
       'products_quantity': ['',[Validators.required]],
       'products_unit': ['',[Validators.required]],
       'products_price': ['',[Validators.required]],
       'default_store': ['',[Validators.required]],
        'products_stocks_lasts': ['',[Validators.required]],
       'products_shelf_life': ['',[Validators.required]],
        'products_expiry_date': ['',[Validators.required]],
       'products_notes': ['',[Validators.required]],
       'products_store': ['',[Validators.required]],
       'products_image': ['',[Validators.required]]

    })
  }

  favoriteLists() {
    this.api.favoriteLists().subscribe(
      (response :any) => {
        this.productsArray = response.data;
      }, (err:any) => {
        console.log(err)
      }
    )
  }
  selectedFile:any= null;
  onFileSelected(event:any){
  this.selectedFile = event.target.files[0];
  }
  getSelectedOptionText(event: any) {
    let selectedOptions = event.target['options'];
    let selectedIndex = selectedOptions.selectedIndex;
    this.selectElementText = selectedOptions[selectedIndex].value;
    
 }
  addProducts() {
   
      const fd = new FormData();
      fd.append('products_image', this.selectedFile , this.selectedFile.name)
      fd.append('products_name',this.products_name)
      fd.append('products_quantity',this.products_quantity)
      fd.append('products_unit',this.products_unit)
      fd.append('products_price',this.products_price)
      fd.append('products_stocks_lasts',this.products_stocks_lasts)
      fd.append('products_shelf_life',this.products_shelf_life)
      fd.append('products_expiry_date',this.products_expiry_date)
      fd.append('products_notes',this.products_notes)
      fd.append('products_category_id',this.id) 
      fd.append('products_sub_cat_id',this.selectElementText)
      if(this.default_store.length >0){
       // fd.append('products_store',this.productsArray[0].products_store)
        fd.append('products_store',this.default_store)
      }
      if(this.default_store == 0){
        fd.append('products_store',this.products_store)
      }

    // alert(JSON.stringify(fd));
    // var data = {};
    // if(this.default_store == 0){
    //   data = {
        
    //     products_category_id: this.id,
    //     products_name: this.products_name,
    //     products_quantity: this.products_quantity,
    //     products_unit: this.products_unit,
    //     products_price: this.products_price,
    //     products_store: this.productsArray[0].products_store,
    //     products_stocks_lasts: this.products_stocks_lasts,
    //     products_shelf_life: this.products_shelf_life,
    //     products_expiry_date: this.products_expiry_date,
    //     products_notes: this.products_notes,
    //     products_image: this.products_image
    //   }
    // }else{
    //   data = {
    //     products_category_id: this.id,
    //     products_name: this.products_name,
    //     products_quantity: this.products_quantity,
    //     products_unit: this.products_unit,
    //     products_price: this.products_price,
    //     products_store: this.default_store,
    //     products_stocks_lasts: this.products_stocks_lasts,
    //     products_shelf_life: this.products_shelf_life,
    //     products_expiry_date: this.products_expiry_date,
    //     products_notes: this.products_notes,
    //     products_image: this.products_image
    //    }  
    // }
     if ((this.products_name == '' || this.products_name == undefined || this.products_name.length < '3' ) ||
      (this.products_quantity == '' || this.products_quantity == undefined)||
      (this.products_price == '' || this.products_price == undefined)||
      (this.products_unit == '' || this.products_unit == undefined)||
      (this.products_stocks_lasts == '' || this.products_stocks_lasts == undefined)||
      (this.products_shelf_life == '' || this.products_shelf_life == undefined)||
      (this.products_notes == '' || this.products_notes == undefined)||
      (this.products_expiry_date == '' || this.products_expiry_date == undefined) || 
      (this.products_sub_cat_id == '0')){
     
        Swal.fire({
          title: 'Oops! Please Enter Valid details',
          text: 'Thanks!',
          icon: 'warning',
         // showCancelButton: true,
          confirmButtonText: 'Ok',
          //cancelButtonText: 'Cancel'
        })
      }else
      {
        
      this.api.addProducts(fd).subscribe(
        (response: any) => {
        var res = response;
        if (res.error == 1) {
          this.loading = false;
          Swal.fire({
            title: 'Oops! Please Enter Valid details',
            text: 'Thanks!',
            icon: 'warning',
            //showCancelButton: true,
            confirmButtonText: 'Ok',
            //cancelButtonText: 'Cancel'
          })
          this.messageError = res.message;
        } else {
          this.loading = false;
          this.messageError = "";
          Swal.fire({
            title: 'Product Added Successfully',
            text: 'You will find product inside category',
            icon: 'success',
            //showCancelButton: true,
            confirmButtonText: 'Ok',
            //cancelButtonText: 'Cancel'
          })
        this.productsArray = response.data;
       

      }
    }, (err: any) => {
      this.loading = false;
        console.log(err)
        Swal.fire({
          title: 'Oops! Please Enter Valid details',
          text: 'Thanks!',
          icon: 'warning',
          //showCancelButton: true,
          confirmButtonText: 'Ok',
          //cancelButtonText: 'Cancel'
        })
        
      }
    )
      }
   
  }

  storeLists() {
    this.api.storeLists().subscribe(
      (response: any) => {
        this.storeListsArray = response.data;
      }, (err: any) => {
        console.log(err)
      }
    )
  }
  subcategoryLists() {
    this.route.queryParams.subscribe((params: { [x: string]: string | undefined; }) => {
      this.id = params['id'];
     });
    var data = {};
  
      data = {
        subCategories_cat_id: this.id
      }
     
    this.api.subcategoryLists(data).subscribe(
      (response: any) => {
        this.subcategoryListsArray = response.data;
      }, (err: any) => {
        console.log(err)
      }
    )
  }

}
